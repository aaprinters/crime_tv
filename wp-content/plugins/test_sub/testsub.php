<?php
/*
Plugin Name: Frontend AJAX Sub-Category Dropdown
Plugin URI: http://khaledsaikat.com
Description: Load sub-category dropdown based on parent category by ajax call
Version: 0.1
Author: Khaled Hossain
Author URI: http://khaledsaikat.com
*/


/**
 * Add shortcode [ajax-dropdown] to any post or page
 */

if ( ! class_exists( 'frontendAjaxDropdown' ) ):

    class frontendAjaxDropdown
    {
        /**
         * Loading WordPress hooks
         */
        function __construct()
        {
            /**
             * Add shortcode function
             */
            add_shortcode( 'ajax-dropdown', array($this, 'init_shortocde') );
            //add_shortcode( 'multi_uplaod', array($this, 'init_multi_uplaod') );

            /**
             * Register ajax action
             */
            add_action( 'wp_ajax_get_subcat', array($this, 'getSubCat') );

            /**
             * Register ajax action for non loged in user
             */
            add_action('wp_ajax_nopriv_get_subcat', array($this, 'getSubCat') );
        }

        /**
         * Show parent dropdown for wordpress category and loaded necessarry javascripts
         */
        function init_multi_uplaod($atts){
			
			if(isset($_POST['submit_memorial'])){
			//$post_title = $_POST['upload_attachment'];
		

        




	}
			?>
			<form action="" method="post" enctype="multipart/form-data">
				<input type="file" name="upload_attachment[]" class="files" size="50" multiple="multiple" />
				<?php //wp_nonce_field( 'upload_attachment', 'my_image_upload_nonce' ); ?>
				<input type="submit" id="submit_memorial" name="submit_memorial" value="<?php if($_GET['id']){ echo 'Save Changes'; }else{ echo 'Create Memorial'; } ?>">

			</form>
			<?php
		}












        function init_shortocde($atts)
        {
			$n = $atts['parent_category'];
			//$$atts = 1;
			ob_start();
			?>
			
			<?php

            echo "<div id='genre_div$n' class='genre_div'>";
            //echo "<div class='genere_btn_main award_button btn'><a class='btn'> Generes</a></div>";
            wp_dropdown_categories(
                'id=main_cat'.$n.'&name=main_cat[]&selected=-1&hierarchical=1&parent='.$n.'&depth=3&hide_empty=0&show_option_none=All Categories'
            );
            ?>
			</div>
            <script type="text/javascript">
                (function($){
					console.log('empty');
                    //$('.sub_cat.award_button.btn').hide();

                    $("<?php echo '#main_cat'.$n; ?>").change(function(){
                        //$('.sub_cat.award_button.btn').show();
                        $("<?php echo '#sub_cat'.$n; ?>").empty();
                    	$.ajax({
                    		type: "post",
                            url: "<?php echo admin_url( 'admin-ajax.php' ); ?>",
                            data: { action: 'get_subcat', cat_id: $("<?php echo '#main_cat'.$n.' option:selected'; ?>").val() },
                    		beforeSend: function() {$("#loading").fadeIn('slow');},

                    		success: function(data) {
                                $("#loading").fadeOut('slow');
                                $("<?php echo '#sub_cat'.$n; ?>").append(data);
                                //$( "#tv_add_post_form .all_cat_section .sub_cat" ).prepend( "<div class='sub_cat_btn award_button btn'><a class='btn'>Sub Generes</a></div>" );
                                console.log("success running");
                    		}
                    	});
                    });
                })(jQuery);
            </script>

            <!--<div id="loading" style="display: none;">Loading...</div>-->
            
            <div class="sub_cat" id="<?php echo 'sub_cat'.$n; ?>"></div>
            <?php
			return ob_get_clean();
        }
		
        /**
        * AJAX action: Shows dropdown for selected parent
        */
        function getSubCat()
        {
            wp_dropdown_categories(
                "name=sub_cat".$n."&selected=-1&hierarchical=1&depth=3&hide_empty=0&child_of={$_POST['cat_id']}"
            );

            die();
        }

    }

endif;

new frontendAjaxDropdown();