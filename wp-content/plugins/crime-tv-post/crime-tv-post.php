<?php
/*
Plugin Name: Crime TV Post
Plugin URI: http://www.wpstriker.com
Description: Plugin for insert crime tv post, use shortcode [crimetvpost]
Version: 1.0.0
Author: wpstriker
Author URI: http://www.wpstriker.com
Text Domain: crime-tv-post-txtdom
Domain Path: languages
Author URI: http://www.wpstriker.com
License: GPLv2
Copyright 2019 wpstriker (email : wpstriker@gmail.com)
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

define( 'CRIME_TV_POST_URL', plugin_dir_url(__FILE__) );
define( 'CRIME_TV_POST_DIR', plugin_dir_path(__FILE__) );
define( 'CRIME_TV_POST_TXTDOM', 'crime-tv-post-txtdom' );

require_once CRIME_TV_POST_DIR . 'functions.php';
require_once CRIME_TV_POST_DIR . 'crime-tv-post_list_table.php';

if( ! class_exists( 'Crime_TV_Post' ) ) :

class Crime_TV_Post {

    public $enable_log  = true;

    public function __construct() {
        $this->init();
    }

    public function init() {
        if( ! session_id() )
            @session_start();
        
        add_action( 'init', array( $this, 'load_plugin_textdomain' ) ); 
        
        add_action( 'wp_enqueue_scripts', array( $this, 'crimetvpost_scripts' ) );

        add_action( 'wp_head', array( $this, 'ajax_url' ) );
        
        add_shortcode( 'crimetvpost', array( $this, 'crimetvpost_callback' ) );
        add_shortcode( 'crimetvpost_step2', array( $this, 'crimetvpost_callback_step2' ) );
        
        add_action( 'wp', array( $this, 'maybe_tv_post_submit' ) );
        
        add_action( 'wp', array( $this, 'maybe_crime_tv_ipn' ) );
        
        add_action( 'wp', array( $this, 'cutstom_debug' ) ); 
        
        add_action( 'admin_menu', array( $this, 'crimetvpost_admin_menu_add' ), 99 );
        
        add_action( 'load-toplevel_page_crimetv_txns', array( $this, 'crimetvpost_add_screen_options' ) );
        
        add_filter( 'set-screen-option', array( $this, 'crimetvpost_set_option' ), 10, 3 );
        
        add_filter( 'manage_posts_columns', array( $this, 'crimetvpost_columns_post_type' ), 10, 1 );
        
        add_action( 'manage_posts_custom_column', array( $this, 'crimetvpost_columns_post_type_render' ), 10, 2 );
        
        add_action( 'add_meta_boxes', array( $this, 'crimetvpost_register_meta_boxes' ) );
        add_filter( 'body_class', array( $this,'multisite_body_classes'));  
    }
    function multisite_body_classes($classes) {
        
         global $post;
        $post_slug = $post->post_name;
        $classes[] = $post_slug;

        return $classes;

}


    public function load_plugin_textdomain() {
        $lang_dir = CRIME_TV_POST_DIR . '/languages/';
        
        // Traditional WordPress plugin locale filter
        $get_locale = get_locale();
    
        if ( version_compare( get_bloginfo( 'version' ), '4.7', '>=' ) ) {
            $get_locale = get_user_locale();
        }
    
        /**
         * Defines the plugin language locale used in plugin.
         *
         * @var string $get_locale The locale to use. Uses get_user_locale()` in WordPress 4.7 or greater,
         *                  otherwise uses `get_locale()`.
         */
        $locale        = apply_filters( 'plugin_locale', $get_locale, CRIME_TV_POST_TXTDOM );
        $mofile        = sprintf( '%1$s-%2$s.mo', CRIME_TV_POST_TXTDOM, $locale );
    
        // Setup paths to current locale file
        $mofile_local  = $lang_dir . $mofile;
        $mofile_global = WP_LANG_DIR . '/' . CRIME_TV_POST_TXTDOM . '/' . $mofile;
    
        if ( file_exists( $mofile_global ) ) {
            // Look in global /wp-content/languages/CRIME_TV_POST_TXTDOM folder
            load_textdomain( CRIME_TV_POST_TXTDOM, $mofile_global );
        } elseif ( file_exists( $mofile_local ) ) {
            // Look in local /wp-content/plugins/CRIME_TV_POST_DIR/languages/ folder
            load_textdomain( CRIME_TV_POST_TXTDOM, $mofile_local );
        } else {
            // Load the default language files
            load_plugin_textdomain( CRIME_TV_POST_TXTDOM, false, $lang_dir );
        }
    }
        
    public function ajax_url() {
        ?><script type="application/javascript">ajaxurl = "<?php echo admin_url( "admin-ajax.php" );?>";</script><?php
    }
    
    public function crimetvpost_scripts() {
        wp_enqueue_style( 'crime-tv-post', CRIME_TV_POST_URL . 'css/crime-tv-post.css' );
        
        wp_enqueue_script( 'jquery' );
        wp_enqueue_script( 'jquery-ui' );
        wp_enqueue_script( 'jquery-ui-core' );
        
        wp_register_script( 'crime-tv-post_js', CRIME_TV_POST_URL . 'js/crime-tv-post.js', array(), '', true );
        wp_enqueue_script( 'crime-tv-post_js' );                    
    }
    
    public function crimetvpost_register_meta_boxes() {
        add_meta_box( 'crimetvpost_meta_box', 'Crime TV Post Data', array( $this, 'crimetvpost_data_display_callback' ), 'post' );
    }
    
    public function crimetvpost_data_display_callback( $post ) {
        
        $post_meta  = get_post_meta( $post->ID );
        //print_r($post->ID);
        
        ?>
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Region</div>
            <div class="it_option">
                <?php 
                echo $post_meta['_tv_region'][0];?>   
            </div>
            <div class="clear"></div>
        </div>        
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">State / Country</div>
            <div class="it_option">
                <?php echo $post_meta['_tv_sub_region'][0];?>   
            </div>
            <div class="clear"></div>
        </div>      
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Company Name</div>
            <div class="it_option">
                <?php echo $post_meta['_company_name'][0];?>    
            </div>
            <div class="clear"></div>
        </div>      <div class="it_option_set layout_option_set">
            <div class="it_option_header">Year of Production</div>
            <div class="it_option">
                <?php echo $post_meta['_year_of_production'][0];?>  
            </div>
            <div class="clear"></div>
        </div>
        
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Status</div>
            <div class="it_option">
                <?php echo $post_meta['_tv_status'][0];?>   
            </div>
            <div class="clear"></div>
        </div>
        
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Post type</div>
            <div class="it_option">
                <?php echo $post_meta['_tv_post_type_screen'][0];?> 
            </div>
            <div class="clear"></div>
        </div>        
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Tv Premier</div>
            <div class="it_option">
                <?php echo $post_meta['_tv_premier'][0];?>  
            </div>
            <div class="clear"></div>
        </div>
        
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Rating magic</div>
            <div class="it_option">
                <?php echo $post_meta['_rating_metric'][0];?>       
            </div>
            <div class="clear"></div>
        </div>
        
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Featured Video</div>
            <div class="it_option">
                <?php echo $post_meta['_featured_video'][0];?>      
            </div>
            <div class="clear"></div>
        </div>


        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Youtube Vimeo Url</div>
            <div class="it_option">
                <?php echo $post_meta['_featured_video_password'][0];?>      
            </div>
            <div class="clear"></div>
        </div>

        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Embed Feat Video: Youtube Vimeo Url</div>
            <div class="it_option">
                <?php echo $post_meta['_embed_featured_video_yvlink'][0];?>      
            </div>
            <div class="clear"></div>
        </div>

        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Featured Video Upload</div>
            <div class="it_option">
                <?php echo $post_meta['_featured_video_upload'][0];?>      
            </div>
            <div class="clear"></div>
        </div>

        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Trailer Video Upload</div>
            <div class="it_option">
                <?php echo $post_meta['_trailer_video_upload'][0];?>      
            </div>
            <div class="clear"></div>
        </div>

        <!--<div class="it_option_set layout_option_set">
            <div class="it_option_header">Featured Image</div>
            <div class="it_option">
                <?php //echo $post_meta['_featured_image'][0];?>      
            </div>
            <div class="clear"></div>
        </div>-->
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">New Photo carousel</div>
            <div class="it_option">
                <?php echo $post_meta['_new_photo_carousel'][0];?>      
            </div>
            <div class="clear"></div>
        </div>



        <!--<div class="it_option_set layout_option_set">
            <div class="it_option_header">Featured Video Custom</div>
            <div class="it_option">
                <?php //echo $post_meta['_featured_video_custom'][0];?>      
            </div>
            <div class="clear"></div>
        </div>-->        
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Featured Image</div>
            <div class="it_option">
                <?php echo $post_meta['_featured_image'][0];?>      
            </div>
            <div class="clear"></div>
        </div>
        
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Trailer</div>
            <div class="it_option">
                <?php echo $post_meta['_tv_trailer'][0];?>  
            </div>
            <div class="clear"></div>
        </div>
        
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Billboard Title</div>
            <div class="it_option">
                <?php echo $post_meta['_tv_billboard_title'][0];?>   
            </div>
            <div class="clear"></div>
        </div>       
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Billboard Subtitle</div>
            <div class="it_option">
                <?php echo $post_meta['_subtitle'][0];?>    
            </div>
            <div class="clear"></div>
        </div>
        
       <!-- <div class="it_option_set layout_option_set">
            <div class="it_option_header">Contents Menu</div>
            <div class="it_option">
                <?php //echo $post_meta['_contents_menu'][0];?>
            </div>
            <div class="clear"></div>
        </div>
        
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Content Title</div>
            <div class="it_option">
                <?php //echo $post_meta['_content_title'][0];?>
            </div>
            <div class="clear"></div>
        </div>-->
        
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Synopsis</div>
            <div class="it_option">
                <?php echo $post_meta['_synopsis'][0];?>
            </div>
            <div class="clear"></div>
        </div>
        
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Features</div>
            <div class="it_option">
                <?php echo $post_meta['_features'][0];?>
            </div>
            <div class="clear"></div>
        </div>
        
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Specifications</div>
            <div class="it_option">
                <?php echo $post_meta['_specifications'][0];?>
            </div>
            <div class="clear"></div>
        </div>        
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Release Date</div>
            <div class="it_option">
                <?php echo $post_meta['_release_date'][0];?>
            </div>
            <div class="clear"></div>
        </div>      
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Tv Season</div>
            <div class="it_option">
                <?php echo $post_meta['_tv_season'][0];?>
            </div>
            <div class="clear"></div>
        </div>      
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Tv Episodes</div>
            <div class="it_option">
                <?php echo $post_meta['_tv_episodes'][0];?>
            </div>
            <div class="clear"></div>
        </div>
        
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Tv Pilot</div>
            <div class="it_option">
                <?php echo $post_meta['_tv_pilot'][0];?>
            </div>
            <div class="clear"></div>
        </div>  
        
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Tv Platform</div>
            <div class="it_option">
                <?php echo $post_meta['_tv_platform'][0];?>
            </div>
            <div class="clear"></div>
        </div>  
        
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Tv Hours</div>
            <div class="it_option">
                <?php echo $post_meta['_tv_hours'][0];?>
            </div>
            <div class="clear"></div>
        </div>  
        
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Tv Minutes</div>
            <div class="it_option">
                <?php echo $post_meta['_tv_minutes'][0];?>
            </div>
            <div class="clear"></div>
        </div>  
        
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Custom Editor</div>
            <div class="it_option">
                <?php echo $post_meta['_mycustomeditor'][0];?>
            </div>
            <div class="clear"></div>
        </div>  
        
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">TV Awards</div>
            <div class="it_option">
                <?php echo $post_meta['_tv_awards'][0];?>
            </div>
            <div class="clear"></div>
        </div>          
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Tv User Bio</div>
            <div class="it_option">
                <?php echo $post_meta['_tv_user_bio_1'][0];?>
            </div>
            <div class="clear"></div>
        </div>  
        
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Tv Credits</div>
            <div class="it_option">
                <?php echo $post_meta['_tv_credits'][0];?>
            </div>
            <div class="clear"></div>
        </div>  

        
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Length</div>
            <div class="it_option">
                <?php echo $post_meta['_length'][0];?>
            </div>
            <div class="clear"></div>
        </div>
        
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Post type</div>
            <div class="it_option">
                <?php echo $post_meta['_tv_post_type'][0];?>
            </div>
            <div class="clear"></div>
        </div>
        
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">User Image</div>
            <div class="it_option">
                <?php echo $post_meta['_tv_user_image'][0];?>   
            </div>
            <div class="clear"></div>
        </div>
        
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">User Bio</div>
            <div class="it_option">
                <?php echo $post_meta['_tv_user_bio'][0];?>
            </div>
            <div class="clear"></div>
        </div>

        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Tv Cast</div>
            <div class="it_option">
                <?php echo $post_meta['tv_cast'][0];?>
            </div>
            <div class="clear"></div>
        </div>
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Tv Cast Image</div>
            <div class="it_option">
                <?php echo $post_meta['tv_cast_image'][0];?>
            </div>
            <div class="clear"></div>
        </div>
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Tv Cast Bio</div>
            <div class="it_option">
                <?php echo $post_meta['tv_cast_bio'][0];?>
            </div>
            <div class="clear"></div>
        </div>

        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Tv Crew</div>
            <div class="it_option">
                <?php echo $post_meta['tv_crew'][0];?>
            </div>
            <div class="clear"></div>
        </div>
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Tv Crew Image</div>
            <div class="it_option">
                <?php echo $post_meta['tv_crew_image'][0];?>
            </div>
            <div class="clear"></div>
        </div>
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Tv Crew Bio</div>
            <div class="it_option">
                <?php echo $post_meta['tv_crew_bio'][0];?>
            </div>
            <div class="clear"></div>
        </div>
        
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Post type</div>
            <div class="it_option">
                <?php echo $post_meta['_tv_post_type_2'][0];?>  
            </div>
            <div class="clear"></div>
        </div>
        
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">User Image</div>
            <div class="it_option">
                <?php echo $post_meta['_tv_user_image_2'][0];?>     
            </div>
            <div class="clear"></div>
        </div>
        
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">User Bio</div>
            <div class="it_option">
                <?php echo $post_meta['_tv_user_bio_2'][0];?>
            </div>
            <div class="clear"></div>
        </div>
        
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">Post type</div>
            <div class="it_option">
                <?php echo $post_meta['_tv_post_type_3'][0];?>  
            </div>
            <div class="clear"></div>
        </div>
        
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">User Image</div>
            <div class="it_option">
                <?php echo $post_meta['_tv_user_image_3'][0];?>
            </div>
            <div class="clear"></div>
        </div>
        
        <div class="it_option_set layout_option_set">
            <div class="it_option_header">User Bio</div>
            <div class="it_option">
                <?php echo $post_meta['_tv_user_bio_3'][0];?>
            </div>
            <div class="clear"></div>
        </div>        
        <?php
    }
    
    public function crimetvpost_admin_menu_add() {
         add_menu_page( 'CrimeTV Txns', 'CrimeTV Txns', 'administrator', 'crimetv_txns', array( $this, 'crimetv_txns_page' ) ); 
    }
    
    public function crimetvpost_add_screen_options() {
        $option = 'per_page';
     
        $args   = array(
            'label'     => 'Payments',
            'default'   => 20,
            'option'    => 'crimetv_txn_per_page'
        );
     
        add_screen_option( $option, $args );
    }

    public function crimetvpost_set_option( $status, $option, $value ) {
        
        if ( 'crimetv_txn_per_page' == $option ) 
            return $value;
     
        return $status; 
    }
    
    public function crimetv_txns_page(){
        global $wpdb;
        
        $crimetv_payment = new CrimeTV_List_Page();
        $crimetv_payment->prepare_items();
        
        ?>
        <div class="wrap">
            
            <div style="clear:both;"></div>
            
            <div id="icon-users" class="icon32"><br/></div>
            <h2>CrimeTV Payment</h2>
                
            <div style="clear:both;"></div>
            
            <form id="images_table-filter" method="get">
                <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
                <?php $crimetv_payment->display() ?>
            </form>
            
        </div>        
        <?php       
    }
    
        public function maybe_tv_post_submit() {
    	if(  isset( $_POST['tv_post_submit'] ) ){
    	//First Form    

        //return;
        global $wpdb;
        global $login_page, $success, $err, $kaya_settings, $success, $current_user;
        $user_profile = get_page_by_path( 'user-profile' );
        $logout_redirect_page = $kaya_settings['logout_redirect_page'] ? $kaya_settings['logout_redirect_page'] : '';
        $user_reg_redirect_page = $kaya_settings['user_reg_redirect_page'] ? $kaya_settings['user_reg_redirect_page'] : ( !empty($user_profile->ID) ? $user_profile->ID : '' );
        $project_name = $_POST['project_name'];
        $email = esc_sql(trim($_POST['user_email']));
        $username = esc_sql(trim($_POST['user_name']));
        $password = esc_sql(trim($_POST['password']));
        $cpassword = esc_sql(trim($_POST['cpassword']));
        //$password = wp_generate_password(12, false);
        if( $email == "" || $username == "") {
            $err = __('Please don\'t leave the required fields.', 'kaya_forms');
        }else if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $err = !empty($kaya_settings['email_valid_error_msg']) ? $kaya_settings['email_valid_error_msg'] : __('Invalid email address.', 'kaya_forms');
        }else if(email_exists($email) ) {
            $err = !empty($kaya_settings['email_exist_error_msg']) ? $kaya_settings['email_exist_error_msg'] : __('Email already exist.', 'kaya_forms');
        }elseif($password != $cpassword){
             $err = __('Passwords Don\'t Match', 'kaya_forms');
        }else {
            add_role( $_POST['user_role_other'], $_POST['user_role_other'], array( 'read' => true, 'edit_posts' => true, 'edit_talents' => true,'upload_files' => true,'edit_project' => true,'create_projects' => true,'read_project' => true ) );
            //$_POST['user_role_name']
            if($_POST['user_role_name'] == 'other_role'){
                if(!empty($_POST['user_role_other'])){
                $user_id = wp_insert_user( array ('user_pass' => apply_filters('pre_user_user_pass', $password), 'user_login' => apply_filters('pre_user_user_login', $username), 'user_email' => apply_filters('pre_user_user_email', $email), 'role' => $_POST['user_role_other']) );
                }else{
                    echo '<p class="kaya-error-message" style="">Please enter other role name</p>';
                    return false;
                }
             // $user_id = wp_insert_user( array ('user_pass' => apply_filters('pre_user_user_pass', $password), 'user_login' => apply_filters('pre_user_user_login', $username), 'user_email' => apply_filters('pre_user_user_email', $email), 'role' => $_POST['user_role_other']) );
            }else{
                $user_id = wp_insert_user( array ('user_pass' => apply_filters('pre_user_user_pass', $password), 'user_login' => apply_filters('pre_user_user_login', $username), 'user_email' => apply_filters('pre_user_user_email', $email),'first_name' =>  $first_name,
            'last_name' =>  $last_name, 'role' => $_POST['user_role_name']) );
            }
            if( is_wp_error($user_id) ) {
                $err = !empty($kaya_settings['reg_error_msg']) ? $kaya_settings['reg_error_msg'] : __('Registration Fail', 'kaya_forms');
            } else {
                $user_role = array();
                if($_POST['user_role_name'] == 'other_role'){
                    $user_role = $_POST['user_role_other'];
                }else{
                    $user_role = $_POST['user_role_name'];
                }
        
            
            $main_cats =$_POST['main_cat'];
            $genre_arr = array();
            foreach($main_cats as $main_cat){
                if($main_cat != '-1')
                {
                    $genre_arr [] = $main_cat;
                }
            }
            $post_category =$_POST['parent_cat'];
            $parent_cat_award = $_POST['parent_cat_award'];
            $sub_genre = $_POST['sub_cat'];
            //$post_categories = array();
            $post_categories = array($post_category,$sub_genre,$parent_cat_award);
            $post_categories_all = array_merge($post_categories,$genre_arr);
                $my_post = array(
                    'post_title'    => $project_name,   
                        'post_content'  => '',
                        'post_status'   => 'publish',
                        'post_author'   => $user_id,
                     );
            // Insert the post into the database
            $post_id = wp_insert_post( $my_post );
            wp_set_post_terms( $post_id, $post_categories_all, 'category' );
            update_user_meta( $user_id, 'talent_post_id', $post_id );                
            wp_set_object_terms( $post_id, $user_role, 'talent_category');
            
			print_r($_POST['year_of_production']);

			update_post_meta( $post_id, 'payment_id', $payment_id );                            
	        update_post_meta( $post_id, '_company_name', $_POST['company_name'] );
	        update_post_meta( $post_id, '_year_of_production', $_POST['year_of_production'] );
	        update_post_meta( $post_id, '_tv_region', $_POST['tv_region'] );
	        update_post_meta( $post_id, '_tv_season', $_POST['tv_season'] );
            update_post_meta( $post_id, '_tv_episodes', $_POST['tv_episodes'] );
            update_post_meta( $post_id, '_tv_pilot', $_POST['tv_pilot'] );
            update_post_meta( $post_id, '_tv_platform', $_POST['tv_platform'] );
            

	        if($_POST['us_states']){
	            update_post_meta( $post_id, '_tv_sub_region', $_POST['us_states'] );
	            update_post_meta( $post_id, 'us_cities', $us_cities );
	        }else{
	            update_post_meta( $post_id, 'other_cities', $other_cities );
	            update_post_meta( $post_id, '_tv_sub_region', $_POST['country'] );
	        }
	        
	        update_post_meta( $post_id, '_tv_status', $_POST['tv_status'] );
	        update_post_meta( $post_id, '_tv_premier', $_POST['tv_premier'] );
	    

            do_action('user_register', $user_id);

            //Auto Login
            $user_data = array();
            $user_data['user_login'] = $username;
            $user_data['user_password'] = $password;
            $user_data['remember'] = true;
            $user_verify = wp_signon( $user_data, true );
            if ( is_wp_error($user_verify) ) {
                $error = $user_verify->get_error_message();
                exit();
            } else {
                wp_set_current_user( $user_verify->ID, $username );
                wp_set_auth_cookie( $user_verify->ID, true, false );
                do_action( 'wp_login', $username );                             
                //wp_redirect(get_the_permalink($user_reg_redirect_page).'?username='.$username);  
                //exit(); 
            }
            }
        }
    
        
        
        /* Payment section */
        
        if( $_POST['tv_payment_method'] == 'stripe' ) {     
            
            require_once CRIME_TV_POST_DIR . 'stripe-php/init.php';
            
            try {               
                \Stripe\Stripe::setApiKey( "sk_test_JlB47fmrnhz52R3TcawYhS9P" );
    
                $token  = \Stripe\Token::create([
                                'card'  => [
                                    'number'    => $_POST['stripe_credit_card_number'],
                                    'exp_month' => $_POST['stripe_credit_card_exp_month'],
                                    'exp_year'  => $_POST['stripe_credit_card_exp_year'],
                                    'cvc'       => $_POST['stripe_credit_card_cvv'],
                                    'currency'  => 'USD',
                                    'name'      => $_POST['stripe_credit_card_name'],
                                  ]
                            ]); 
                
                $charge = \Stripe\Charge::create([
                              "amount"      => 1000,
                              "currency"    => 'USD',
                              "source"      => $token->id, // obtained with Stripe.js
                              "description" => 'CrimeTVPost'
                            ]);

            } catch(\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught
                $body = $e->getJsonBody();
                $err  = $body['error'];
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly
                $body = $e->getJsonBody();
                $err  = $body['error'];
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API
                $body = $e->getJsonBody();
                $err  = $body['error'];
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed
                // (maybe you changed API keys recently)
                $body = $e->getJsonBody();
                $err  = $body['error'];
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed
                $body = $e->getJsonBody();
                $err  = $body['error'];
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send
                // yourself an email
                $body = $e->getJsonBody();
                $err  = $body['error'];
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe
                $body = $e->getJsonBody();
                $err  = $body['error'];
            }
            
            
            if( $charge && $charge->paid ) {
                $_SESSION['payment_charge'] = $charge; 
            } else {
                $_SESSION['payment_fail']   = true; 
                $_SESSION['payment_error']  = $err['message'] ? 'ERROR: ' . $err['message'] : 'ERROR: Payment failed, please try again.';
                wp_redirect( ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );
                die();
            }
            
        }
                                        
            $payment_data   = array(
                                'wp_user_id'        => $wp_user_id,
                                'post_id'           => $post_id,
                                'amount'            => '10.00',
                                'payment_method'    => $_POST['tv_payment_method'],
                                'transaction_id'    => $_POST['tv_payment_method'] == 'stripe' ? $charge->id : '',
                                'payment_status'    => $_POST['tv_payment_method'] == 'stripe' ? 'Completed' : 'Pending',
                                'updated_at'        => current_time( 'mysql' ),
                                'created_at'        => current_time( 'mysql' ),
                                'ip'                => get_ip(),
                                );
                                            
            $wpdb->insert( $wpdb->prefix . 'crimetv_payment', $payment_data );
            $payment_id = $wpdb->insert_id; 
            
			
            if( $_POST['tv_payment_method'] == 'paypal' ) {
            $paypal_email   = 'jlrangani.28-facilitator@gmail.com';
            $first_name     = get_user_meta( $wp_user_id, 'first_name', true );
            $last_name      = get_user_meta( $wp_user_id, 'last_name', true );
        
            $item_name      = 'CrimeTV Payment';
            $item_amount    = 10;
            $item_amount    = number_format( $item_amount, 2 );
            
            $querystring    = '';
            
            // Firstly Append paypal account to querystring
            $querystring .= "?business=" . urlencode( $paypal_email ) . "&";
            
            //The item name and amount can be brought in dynamically by querying the $_POST['item_number'] variable.
            $querystring .= "item_name=" . urlencode( $item_name ) . "&";
            $querystring .= "amount=" . urlencode( $item_amount ) . "&";
            
            $_postdata  = array(
                            'cmd'           => '_xclick',
                            'no_note'       => '1',
                            'lc'            => 'US',
                            'currency_code' => 'USD',
                            'bn'            => 'PP-BuyNowBF:btn_buynow_LG.gif:NonHostedGuest',
                            'first_name'    => $first_name,
                            'last_name'     => $last_name,
                            'payer_email'   => '',
                            //'item_number' => '231'
                            );
            
            //loop for posted values and append to querystring
            foreach( $_postdata as $key => $value ){
                $value      = urlencode( stripslashes( $value ) );
                $querystring .= "$key=$value&";
            }
            
            $return_url     = add_query_arg( 'pg', 'crimetvreturn', site_url( '/' ) );
            $cancel_url     = add_query_arg( 'pg', 'crimetvcancel', site_url( '/' ) );
            $notify_url     = add_query_arg( 'pg', 'crimetvipn', site_url( '/' ) );
             
            // Append paypal return addresses
            $querystring .= 'notify_url=' . urlencode( stripslashes( esc_url_raw( $notify_url ) ) ) . '&';
            $querystring .= 'return=' . urlencode( stripslashes( esc_url_raw( $return_url ) ) ) . '&';
            $querystring .= 'cancel_return=' . urlencode( stripslashes( esc_url_raw( $cancel_url ) ) );
                    
            // Append querystring with custom field
            $custom_data    = $payment_id . "|" . $post_id . "|" . $wp_user_id;
            $custom_data    = urlencode( stripslashes( $custom_data ) );    
            $querystring .= "&custom=" . $custom_data;
            
            // Redirect to paypal IPN
            wp_redirect( 'https://www.sandbox.paypal.com/cgi-bin/webscr' . $querystring );              
        } else if( $_POST['tv_payment_method'] == 'stripe' ) {
            update_post_meta( $post_id, 'charge_id', $charge->id );
            //wp_redirect( get_permalink( $post_id ) );                   
        }
        
        //die();
        
        }
        // Second Form
        if(  isset( $_POST['tv_save_post_data'] ) ){
        
        //if( ! is_wp_error( $post_id ) ) {
            // Update post meta 
            //$_POST
        
        //Carousel Uplaod Isset Starts 
        
        if (isset($_FILES['upload_attachment'])) {

            require_once( ABSPATH . 'wp-admin/includes/image.php' );
            require_once( ABSPATH . 'wp-admin/includes/file.php' );
            require_once( ABSPATH . 'wp-admin/includes/media.php' );


            $files = $_FILES['upload_attachment'];
            $count = 0;
            $galleryImages = array();


            foreach ($files['name'] as $count => $value) {

				if ($files['name'][$count]) {

					$file = array(
						'name'     => $files['name'][$count],
						'type'     => $files['type'][$count],
						'tmp_name' => $files['tmp_name'][$count],
						'error'    => $files['error'][$count],
						'size'     => $files['size'][$count]
					);

					$upload_overrides = array( 'test_form' => false );
					$upload = wp_handle_upload($file, $upload_overrides);


					// $filename should be the path to a file in the upload directory.
					$filename = $upload['file'];

					// The ID of the post this attachment is for.
					$parent_post_id = $post_id;

					// Check the type of tile. We'll use this as the 'post_mime_type'.
					$filetype = wp_check_filetype( basename( $filename ), null );

					// Get the path to the upload directory.
					$wp_upload_dir = wp_upload_dir();

					// Prepare an array of post data for the attachment.
					$attachment = array(
						'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ), 
						'post_mime_type' => $filetype['type'],
						'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
						'post_content'   => '',
						'post_status'    => 'inherit'
					);

					// Insert the attachment.
					$attach_id = wp_insert_attachment( $attachment, $filename, $parent_post_id );

					// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
					require_once( ABSPATH . 'wp-admin/includes/image.php' );

					// Generate the metadata for the attachment, and update the database record.
					$attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
					wp_update_attachment_metadata( $attach_id, $attach_data );

					array_push($galleryImages, $attach_id);

				}

				$count++;
			}
			
				

				$user_id = get_current_user_id();
				$post_id = get_user_meta( $user_id, 'talent_post_id', true);
				$gal_imagees_urls = implode(",",$galleryImages);
				update_post_meta( $post_id, '_new_photo_carousel', $gal_imagees_urls );
		}

        //Carousel Uplaod Isset	Ends

          
        if(is_user_logged_in()){
            $user_id = get_current_user_id();
            $post_id = get_user_meta( $user_id, 'talent_post_id', true);
        
            
            ?><pre><?php print_r($_POST['project_name']); ?></pre>

            <div class="alert alert-success" role="alert">
              Data has Been Saved Successfully!
            </div><?php

            update_post_meta( $post_id, '_rating_metric', $_POST['tv_rating_metric'] );
            update_post_meta( $post_id, '_featured_video', $_POST['tv_featured_video'] );

            update_post_meta( $post_id, '_featured_video_password', $_POST['tv_featured_video_password'] );
            update_post_meta( $post_id, '_trailer_video_upload', $_POST['preview1-multi-929-0'] );

            update_post_meta( $post_id, '_embed_featured_video_yvlink', $_POST['embed_featured_video_yvlink'] );
            update_post_meta( $post_id, '_featured_video_upload', $_POST['preview1-multi-912-0'] );
            
            update_post_meta( $post_id, '_featured_image', $_POST['preview1-multi-927-0'] );

            //set featured image
            $attachment_id = attachment_url_to_postid( $_POST['preview1-multi-927-0'] );            
            set_post_thumbnail($post_id, $attachment_id);

            

            update_post_meta( $post_id, '_featured_video_custom', $_POST['preview1-multi-912-0'] );
            //update_post_meta( $post_id, '_featured_image', $_POST['tv_featured_image'] );
            update_post_meta( $post_id, '_post_layout', 'billboard' );
            update_post_meta( $post_id, '_sidebar_position', 'sidebar-right' );
            update_post_meta( $post_id, '_featured_image_size', 'none' );
            update_post_meta( $post_id, '_disable_review', true );          
            
            update_post_meta( $post_id, '_custom_fest_categories', $_POST['tv-custom-fest-categories'] );
            update_post_meta( $post_id, '_standard_categories', $_POST['tv-standard-categories'] );
            
            update_post_meta( $post_id, '_synopsis', $_POST['tv_synopsis'] );
            update_post_meta( $post_id, '_features', $_POST['tv_features'] );
            update_post_meta( $post_id, '_specifications', $_POST['tv_specifications'] );
            update_post_meta( $post_id, '_release_date', $_POST['release_date'] );
            
            update_post_meta( $post_id, '_length', $_POST['tv_length'] );
            update_post_meta( $post_id, '_tv_billboard_title', $_POST['tv_billboard_title'] );
            update_post_meta( $post_id, '_subtitle', $_POST['tv_billboard_subtitle'] );
            update_post_meta( $post_id, '_article_title', $_POST['tv_content_title'] );
            
            //update_post_meta( $post_id, '_contents_menu', $_POST['tv_contents_menu'] );
            //update_post_meta( $post_id, '_content_title', $_POST['project_name'] );

            update_post_meta( $post_id, '_tv_hours', $_POST['tv_hours'] );
            update_post_meta( $post_id, '_tv_minutes', $_POST['tv_minutes'] );
            
            update_post_meta( $post_id, '_tv_trailer', $_POST['preview1-multi-929-0'] );
            update_post_meta( $post_id, '_tv_post_type_screen', $_POST['tv_post_type_screen'] );
            update_post_meta( $post_id, '_tv_premier', $_POST['tv_premier'] );            
            update_post_meta( $post_id, '_mycustomeditor', $_POST['mycustomeditor'] );
            update_post_meta( $post_id, '_tv_awards', $_POST['tv_awards'] );
            //update_post_meta( $post_id, '_tv_user_bio_1', $_POST['tv_user_bio_1'] );
            update_post_meta( $post_id, '	', $_POST['tv_credits'] );
            
            // Update post Content
		    $update_post = [
		      'ID'           => $post_id,
		      'post_content' => $_POST['mycustomeditor'],
		    ];
		    // Update the post into the database
		    wp_update_post( $update_post );

            $tv_cast = implode($_POST['tv_cast'],',');
            update_post_meta( $post_id, 'tv_cast', $tv_cast );
            $tv_cast_image = implode($_POST['tv_cast_image'],',');
            update_post_meta( $post_id, 'tv_cast_image', $tv_cast_image );
            $tv_cast_bio = implode($_POST['tv_cast_bio'],',');
            update_post_meta( $post_id, 'tv_cast_bio', $tv_cast_bio );
             
            $tv_crew = implode($_POST['tv_crew'],',');
            update_post_meta( $post_id, 'tv_crew', $tv_crew );
            $tv_crew_image = implode($_POST['tv_crew_image'],',');
            update_post_meta( $post_id, 'tv_crew_image', $tv_crew_image );
            $tv_crew_bio = implode($_POST['tv_crew_bio'],',');
            update_post_meta( $post_id, 'tv_crew_bio', $tv_crew_bio );
            $main_content =  $_POST['mycustomeditor'];
            $my_post['post_content'] = $main_content;
            $my_post = array();
            wp_update_post( $my_post );
            
            /* update_post_meta( $post_id, '_tv_post_type_1', $tv_crew_bio );
            update_post_meta( $post_id, '_tv_post_type_1', $_POST['tv_post_type_1'] );
            update_post_meta( $post_id, '_tv_post_type_2', $_POST['tv_post_type_2'] );
            update_post_meta( $post_id, '_tv_user_bio_2', $_POST['tv_user_bio_2'] );
            update_post_meta( $post_id, '_tv_post_type_3', $_POST['tv_post_type_3'] );
            update_post_meta( $post_id, '_tv_user_bio_3', $_POST['tv_user_bio_3'] ); */
            
                
            if ( ! function_exists( 'wp_generate_attachment_metadata' ) ) {
                require_once( ABSPATH . "wp-admin" . '/includes/image.php' );
                require_once( ABSPATH . "wp-admin" . '/includes/file.php' );
                require_once( ABSPATH . "wp-admin" . '/includes/media.php' );
            }
            
            if ( $_FILES['tv_background_image'] && $_FILES['tv_background_image']['error'] === UPLOAD_ERR_OK ) {
                $post_attach_id     = media_handle_upload( 'tv_background_image', $post_id );
                
                if ( ! is_wp_error( $post_attach_id ) && $post_attach_id > 0 ) {
                    update_post_meta( $post_id, '_thumbnail_id', $post_attach_id );
                    update_post_meta( $post_id, '_bg_image_id', $post_attach_id );
                    update_post_meta( $post_id, '_bg_image', wp_get_attachment_url( $post_attach_id ) );
                }                               
            }
            
            if ( $_FILES['tv_user_image_1'] && $_FILES['tv_user_image_1']['error'] === UPLOAD_ERR_OK ) {
                $tv_user_image_1    = media_handle_upload( 'tv_user_image_1', $post_id );
                
                if ( ! is_wp_error( $tv_user_image_1 ) && $tv_user_image_1 > 0 ) {
                    update_post_meta( $post_id, '_tv_user_image_1_id', $tv_user_image_1 );
                    update_post_meta( $post_id, '_tv_user_image_1', wp_get_attachment_url( $tv_user_image_1 ) );
                }                               
            }
            
            if ( $_FILES['tv_user_image_2'] && $_FILES['tv_user_image_2']['error'] === UPLOAD_ERR_OK ) {
                $tv_user_image_2    = media_handle_upload( 'tv_user_image_2', $post_id );
                
                if ( ! is_wp_error( $tv_user_image_2 ) && $tv_user_image_2 > 0 ) {
                    update_post_meta( $post_id, '_tv_user_image_2_id', $tv_user_image_2 );
                    update_post_meta( $post_id, '_tv_user_image_2', wp_get_attachment_url( $tv_user_image_2 ) );
                }                               
            }
            
            if ( $_FILES['tv_user_image_3'] && $_FILES['tv_user_image_3']['error'] === UPLOAD_ERR_OK ) {
                $tv_user_image_3    = media_handle_upload( 'tv_user_image_3', $post_id );
                
                if ( ! is_wp_error( $tv_user_image_3 ) && $tv_user_image_3 > 0 ) {
                    update_post_meta( $post_id, '_tv_user_image_3_id', $tv_user_image_3 );
                    update_post_meta( $post_id, '_tv_user_image_3', wp_get_attachment_url( $tv_user_image_3 ) );
                }                               
            }
            
            
            }
        }
    }
    
    
    public function maybe_crime_tv_ipn() {
        if( ! isset( $_REQUEST['pg'] ) || $_REQUEST['pg'] != 'crimetvipn' )
            return;
        
        $this->log( 'Inside IPN' ); 
                
        // Get received values from post data.
        $posted        = wp_unslash( $_POST ); // WPCS: CSRF ok, input var ok.
        
        $this->log( json_encode( $posted ) );   
        
        $posted['cmd'] = '_notify-validate';
        
        // Send back post vars to paypal.
        $params = array(
            'body'        => $posted,
            'timeout'     => 60,
            'httpversion' => '1.1',
            'compress'    => false,
            'decompress'  => false,
            'user-agent'  => 'Wordpress IPN',
        );

        // Post back to get a response.
        $response = wp_safe_remote_post( 'https://www.sandbox.paypal.com/cgi-bin/webscr', $params );

        $this->log( $response );    
        
        // Check to see if the request was valid.
        if ( is_wp_error( $response ) ) {
            $this->log( 'Error response: ' . $response->get_error_message() );
        } else if ( ! is_wp_error( $response ) && $response['response']['code'] >= 200 && $response['response']['code'] < 300 && strstr( $response['body'], 'VERIFIED' ) ) {
            $this->log( 'Received valid response from PayPal IPN' );
            
            // Lowercase returned variables.
            $posted['payment_status']   = strtolower( $posted['payment_status'] );
            
            global $wpdb;
            
            $order_txn_id       = $posted['txn_id'];            
            $custom             = $posted['custom'];            
            $custom             = explode( '|', $custom );
            $payment_id         = $custom[0];
            $post_id            = $custom[1];
            $user_id            = $custom[2];
                        
            $wpdb->update( $wpdb->prefix . 'crimetv_payment', array( 'payment_status' => ucfirst( $posted['payment_status'] ), 'transaction_id' => $order_txn_id ), array( 'id' => $payment_id ) );
            
            $to         = get_option( 'admin_email' );      
            
            $subject    = "Payment Success for post item: " . $post_id;
            
            $body       = "Payment Method : Paypal<br>";
            $body      .= "Transaction ID : " . $order_txn_id . "<br>";
            $body      .= "Post Edit Link : <strong><a href='" . get_edit_post_link( $post_id ) . "' target='_blank'>" . get_edit_post_link( $post_id ) . "</a></strong><br>";
            $body      .= "Post ID : " . $post_id;
            $body      .= "User ID : " . $user_id;
            
            $this->send_mail( $to, $subject, $body );       
        } else {
            $this->log( 'Received invalid response from PayPal IPN' );
        }
                        
        exit;           
    }
    
public function render_add_post_form_step2 ( $values = array() ) {

}
public function render_add_post_form ( $values = array() ) {
        
        if( $_SESSION['payment_fail'] ) {
            echo '<div class="tv-post-error">' . $_SESSION['payment_error'] . '</div>';
            
            $_SESSION['payment_fail']   = NULL; 
            $_SESSION['payment_error']  = NULL;                 
        }
    if(!is_user_logged_in()){
            ?>
        <div class="tv-post-wrapper container">
            <form action="" id="tv_add_post_form" name="tv_add_post_form" method="post" enctype="multipart/form-data">
            <div class="all_cat_section">           
            <div class="award_button select_category_head"><a class="btn" >Select Category</a></div>

            <?php
            $taxonomies = get_terms( array(
    'taxonomy' => 'category',
    'hide_empty' => false
) );
 
if ( !empty($taxonomies) ) :
/*    
    foreach( $taxonomies as $category ) {
        if( $category->parent == 0 ) {
            $output1.= '<div label="'. esc_attr( $category->name ) . '">'.$category->name.'</div>';
        }
        }
    echo $output1; */
    $count = 1;
    $output = '<div class="category_container row">';
    foreach( $taxonomies as $category ) {
        if( $category->parent == 0 ) {
            
      $filter_allowed_arrays = array('Film','Network/Cable TV','Streaming','Shorts','Video Games and New Media');   
        //$filter_allowed_arrays = array('Shorts');   
        $category_slug = $category->name; 
        
        if (in_array($category_slug, $filter_allowed_arrays )) {
                    $size = "medium";
                    if (function_exists('ttw_thumbnail_url')){ 
                        $cat_thumb1 = ttw_thumbnail_url($category->term_id,$size);
                    }
                $output.= '
                <div class="porthole terms_images_main col-sm-2">
                    <div class="crime_selection_label" data-value="'.$category->term_id.'" data-name="'.$category->name.'">
                    
                        <div class="porthole-layer" style="opacity: 0.9;"></div>
                        <div class="porthole-color" style="opacity: 0;"></div>
                        
                        <div class="porthole-info">
                            <div class="porthole-inner">
                            
                                <div class="meter-circle-wrapper">
                                    <div class="meter-circle porthole-circle">
                                    <div class="cat_img image" style="background-image: url('.$cat_thumb1.')"></div>
                                    </div>
                                </div>
                            <div class="article-info"><h3><p label="">'.esc_attr( $category->name ).'</p></h3></div>
                            
                            </div>
                        </div>
                    </div>';
                    $sub_genres_arr = array();
                    $output.= '<div class="crime_selection_dropdown"><div label="'. esc_attr( $category->name ) .'">';
                    $parent_category = $category->term_id;
                    
                   
                    echo do_shortcode('[ajax-dropdown parent_category ="'.$parent_category.'"]');
                   
                    $output.='</div></div>
                </div>';
        }
        }
        $count++;
        
        
        
    }
    $output.='</div>';
    $output.='<input type="text" id="parent_cat" name="parent_cat" value="" style="display: none;">';
    $output.='<input type="text" id="parent_cat_award" name="parent_cat_award" value="" style="display: none;">';
    

    echo $output;
    $audience_category = get_term_by('name', 'Audience Awards', 'category');
    
    $parent_category_award = $audience_category->term_id;
    echo '<div class="cat_btn award_button btn"><a class="btn">Generes</a></div>';
    echo '<div class="sub_cat_btn award_button btn"><a class="btn">Sub Generes</a></div>';
    echo '<div class="audience_awds"><a class="btn" id="audience_awards" data-value="'.$parent_category_award.'">Audience Awards</a></div>';
    echo '<div id="parent_category_award" style="display:none;">';
    //echo '<div class="tv-post-label">Audience Awards</div>';
    echo do_shortcode('[ajax-dropdown parent_category ="'.$parent_category_award.'"]');
    echo '</div>';
    
endif;
?>


                <div class="empty_space"></div>
                <div class="tv-post-row">
                    <div class="tv-post-label">Project Name</div>
                    <div class="tv-post-input form-group">
                            <input type="text" class="form-control" name="project_name" id="project_name" value="" />
                    </div>
                </div>   
                <div class="tv-post-row">
                <div class="tv-post-label">Region</div>
                <div class="tv-post-input">
                    
                    <div class="tv-radio-row row">
                        
                    <div class="col col-sm-2">
                        <div class="radio">
                          <label for="tv_region_1"><input type="radio" name="tv_region" id="tv_region_1" value="United States">United States</label>
                        </div>
                    </div>

                    <div class="col col-sm-8">
                       <!-- <label for="tv_region_1"><input type="radio" name="tv_region" id="tv_region_1" value="United States" />United States</label> -->
                         
                    <div class="united_states_america form-group" style="display: none;">                       
                        <select name="us_states" class="form-control" id="us_states">
                            <option value="">Select State</option>
                            <option value="AL">Alabama</option>
                            <option value="AK">Alaska</option>
                            <option value="AZ">Arizona</option>
                            <option value="AR">Arkansas</option>
                            <option value="CA">California</option>
                            <option value="CO">Colorado</option>
                            <option value="CT">Connecticut</option>
                            <option value="DE">Delaware</option>
                            <option value="DC">District Of Columbia</option>
                            <option value="FL">Florida</option>
                            <option value="GA">Georgia</option>
                            <option value="HI">Hawaii</option>
                            <option value="ID">Idaho</option>
                            <option value="IL">Illinois</option>
                            <option value="IN">Indiana</option>
                            <option value="IA">Iowa</option>
                            <option value="KS">Kansas</option>
                            <option value="KY">Kentucky</option>
                            <option value="LA">Louisiana</option>
                            <option value="ME">Maine</option>
                            <option value="MD">Maryland</option>
                            <option value="MA">Massachusetts</option>
                            <option value="MI">Michigan</option>
                            <option value="MN">Minnesota</option>
                            <option value="MS">Mississippi</option>
                            <option value="MO">Missouri</option>
                            <option value="MT">Montana</option>
                            <option value="NE">Nebraska</option>
                            <option value="NV">Nevada</option>
                            <option value="NH">New Hampshire</option>
                            <option value="NJ">New Jersey</option>
                            <option value="NM">New Mexico</option>
                            <option value="NY">New York</option>
                            <option value="NC">North Carolina</option>
                            <option value="ND">North Dakota</option>
                            <option value="OH">Ohio</option>
                            <option value="OK">Oklahoma</option>
                            <option value="OR">Oregon</option>
                            <option value="PA">Pennsylvania</option>
                            <option value="RI">Rhode Island</option>
                            <option value="SC">South Carolina</option>
                            <option value="SD">South Dakota</option>
                            <option value="TN">Tennessee</option>
                            <option value="TX">Texas</option>
                            <option value="UT">Utah</option>
                            <option value="VT">Vermont</option>
                            <option value="VA">Virginia</option>
                            <option value="WA">Washington</option>
                            <option value="WV">West Virginia</option>
                            <option value="WI">Wisconsin</option>
                            <option value="WY">Wyoming</option>             
                        </select>                       
                      </div>
                      
                    </div>                    
                    <div class="col col-sm-2">
                        <div class="form-group" id="us_cities" style="display: none;">
                            <input type="text" value="" class="form-control" name="us_cities" placeholder="US City">
                        </div>
                    </div>
                    
                    </div>

                    <div class="tv-radio-row">
                    
                    <!--<label for="tv_region_2"><input type="radio" name="tv_region" id="tv_region_2" value="International" />International</label>-->
            
                    <div class="row">

                    <div class="col col-sm-2">
                        <div class="radio">
                            <label for="tv_region_2"><input type="radio" name="tv_region" id="tv_region_2" value="International">International</label>
                        </div>
                    </div>

                    <div class="col col-sm-8">
                        <div class="radio_international_main form-group" style="display: none;">
                            <select class="form-control" id="country" name="country">
                            <option value="">Select Country</option>
                               <option value="Afganistan">Afghanistan</option>
                               <option value="Albania">Albania</option>
                               <option value="Algeria">Algeria</option>
                               <option value="American Samoa">American Samoa</option>
                               <option value="Andorra">Andorra</option>
                               <option value="Angola">Angola</option>
                               <option value="Anguilla">Anguilla</option>
                               <option value="Antigua & Barbuda">Antigua & Barbuda</option>
                               <option value="Argentina">Argentina</option>
                               <option value="Armenia">Armenia</option>
                               <option value="Aruba">Aruba</option>
                               <option value="Australia">Australia</option>
                               <option value="Austria">Austria</option>
                               <option value="Azerbaijan">Azerbaijan</option>
                               <option value="Bahamas">Bahamas</option>
                               <option value="Bahrain">Bahrain</option>
                               <option value="Bangladesh">Bangladesh</option>
                               <option value="Barbados">Barbados</option>
                               <option value="Belarus">Belarus</option>
                               <option value="Belgium">Belgium</option>
                               <option value="Belize">Belize</option>
                               <option value="Benin">Benin</option>
                               <option value="Bermuda">Bermuda</option>
                               <option value="Bhutan">Bhutan</option>
                               <option value="Bolivia">Bolivia</option>
                               <option value="Bonaire">Bonaire</option>
                               <option value="Bosnia & Herzegovina">Bosnia & Herzegovina</option>
                               <option value="Botswana">Botswana</option>
                               <option value="Brazil">Brazil</option>
                               <option value="British Indian Ocean Ter">British Indian Ocean Ter</option>
                               <option value="Brunei">Brunei</option>
                               <option value="Bulgaria">Bulgaria</option>
                               <option value="Burkina Faso">Burkina Faso</option>
                               <option value="Burundi">Burundi</option>
                               <option value="Cambodia">Cambodia</option>
                               <option value="Cameroon">Cameroon</option>
                               <option value="Canada">Canada</option>
                               <option value="Canary Islands">Canary Islands</option>
                               <option value="Cape Verde">Cape Verde</option>
                               <option value="Cayman Islands">Cayman Islands</option>
                               <option value="Central African Republic">Central African Republic</option>
                               <option value="Chad">Chad</option>
                               <option value="Channel Islands">Channel Islands</option>
                               <option value="Chile">Chile</option>
                               <option value="China">China</option>
                               <option value="Christmas Island">Christmas Island</option>
                               <option value="Cocos Island">Cocos Island</option>
                               <option value="Colombia">Colombia</option>
                               <option value="Comoros">Comoros</option>
                               <option value="Congo">Congo</option>
                               <option value="Cook Islands">Cook Islands</option>
                               <option value="Costa Rica">Costa Rica</option>
                               <option value="Cote DIvoire">Cote DIvoire</option>
                               <option value="Croatia">Croatia</option>
                               <option value="Cuba">Cuba</option>
                               <option value="Curaco">Curacao</option>
                               <option value="Cyprus">Cyprus</option>
                               <option value="Czech Republic">Czech Republic</option>
                               <option value="Denmark">Denmark</option>
                               <option value="Djibouti">Djibouti</option>
                               <option value="Dominica">Dominica</option>
                               <option value="Dominican Republic">Dominican Republic</option>
                               <option value="East Timor">East Timor</option>
                               <option value="Ecuador">Ecuador</option>
                               <option value="Egypt">Egypt</option>
                               <option value="El Salvador">El Salvador</option>
                               <option value="Equatorial Guinea">Equatorial Guinea</option>
                               <option value="Eritrea">Eritrea</option>
                               <option value="Estonia">Estonia</option>
                               <option value="Ethiopia">Ethiopia</option>
                               <option value="Falkland Islands">Falkland Islands</option>
                               <option value="Faroe Islands">Faroe Islands</option>
                               <option value="Fiji">Fiji</option>
                               <option value="Finland">Finland</option>
                               <option value="France">France</option>
                               <option value="French Guiana">French Guiana</option>
                               <option value="French Polynesia">French Polynesia</option>
                               <option value="French Southern Ter">French Southern Ter</option>
                               <option value="Gabon">Gabon</option>
                               <option value="Gambia">Gambia</option>
                               <option value="Georgia">Georgia</option>
                               <option value="Germany">Germany</option>
                               <option value="Ghana">Ghana</option>
                               <option value="Gibraltar">Gibraltar</option>
                               <option value="Great Britain">Great Britain</option>
                               <option value="Greece">Greece</option>
                               <option value="Greenland">Greenland</option>
                               <option value="Grenada">Grenada</option>
                               <option value="Guadeloupe">Guadeloupe</option>
                               <option value="Guam">Guam</option>
                               <option value="Guatemala">Guatemala</option>
                               <option value="Guinea">Guinea</option>
                               <option value="Guyana">Guyana</option>
                               <option value="Haiti">Haiti</option>
                               <option value="Hawaii">Hawaii</option>
                               <option value="Honduras">Honduras</option>
                               <option value="Hong Kong">Hong Kong</option>
                               <option value="Hungary">Hungary</option>
                               <option value="Iceland">Iceland</option>
                               <option value="Indonesia">Indonesia</option>
                               <option value="India">India</option>
                               <option value="Iran">Iran</option>
                               <option value="Iraq">Iraq</option>
                               <option value="Ireland">Ireland</option>
                               <option value="Isle of Man">Isle of Man</option>
                               <option value="Israel">Israel</option>
                               <option value="Italy">Italy</option>
                               <option value="Jamaica">Jamaica</option>
                               <option value="Japan">Japan</option>
                               <option value="Jordan">Jordan</option>
                               <option value="Kazakhstan">Kazakhstan</option>
                               <option value="Kenya">Kenya</option>
                               <option value="Kiribati">Kiribati</option>
                               <option value="Korea North">Korea North</option>
                               <option value="Korea Sout">Korea South</option>
                               <option value="Kuwait">Kuwait</option>
                               <option value="Kyrgyzstan">Kyrgyzstan</option>
                               <option value="Laos">Laos</option>
                               <option value="Latvia">Latvia</option>
                               <option value="Lebanon">Lebanon</option>
                               <option value="Lesotho">Lesotho</option>
                               <option value="Liberia">Liberia</option>
                               <option value="Libya">Libya</option>
                               <option value="Liechtenstein">Liechtenstein</option>
                               <option value="Lithuania">Lithuania</option>
                               <option value="Luxembourg">Luxembourg</option>
                               <option value="Macau">Macau</option>
                               <option value="Macedonia">Macedonia</option>
                               <option value="Madagascar">Madagascar</option>
                               <option value="Malaysia">Malaysia</option>
                               <option value="Malawi">Malawi</option>
                               <option value="Maldives">Maldives</option>
                               <option value="Mali">Mali</option>
                               <option value="Malta">Malta</option>
                               <option value="Marshall Islands">Marshall Islands</option>
                               <option value="Martinique">Martinique</option>
                               <option value="Mauritania">Mauritania</option>
                               <option value="Mauritius">Mauritius</option>
                               <option value="Mayotte">Mayotte</option>
                               <option value="Mexico">Mexico</option>
                               <option value="Midway Islands">Midway Islands</option>
                               <option value="Moldova">Moldova</option>
                               <option value="Monaco">Monaco</option>
                               <option value="Mongolia">Mongolia</option>
                               <option value="Montserrat">Montserrat</option>
                               <option value="Morocco">Morocco</option>
                               <option value="Mozambique">Mozambique</option>
                               <option value="Myanmar">Myanmar</option>
                               <option value="Nambia">Nambia</option>
                               <option value="Nauru">Nauru</option>
                               <option value="Nepal">Nepal</option>
                               <option value="Netherland Antilles">Netherland Antilles</option>
                               <option value="Netherlands">Netherlands (Holland, Europe)</option>
                               <option value="Nevis">Nevis</option>
                               <option value="New Caledonia">New Caledonia</option>
                               <option value="New Zealand">New Zealand</option>
                               <option value="Nicaragua">Nicaragua</option>
                               <option value="Niger">Niger</option>
                               <option value="Nigeria">Nigeria</option>
                               <option value="Niue">Niue</option>
                               <option value="Norfolk Island">Norfolk Island</option>
                               <option value="Norway">Norway</option>
                               <option value="Oman">Oman</option>
                               <option value="Pakistan">Pakistan</option>
                               <option value="Palau Island">Palau Island</option>
                               <option value="Palestine">Palestine</option>
                               <option value="Panama">Panama</option>
                               <option value="Papua New Guinea">Papua New Guinea</option>
                               <option value="Paraguay">Paraguay</option>
                               <option value="Peru">Peru</option>
                               <option value="Phillipines">Philippines</option>
                               <option value="Pitcairn Island">Pitcairn Island</option>
                               <option value="Poland">Poland</option>
                               <option value="Portugal">Portugal</option>
                               <option value="Puerto Rico">Puerto Rico</option>
                               <option value="Qatar">Qatar</option>
                               <option value="Republic of Montenegro">Republic of Montenegro</option>
                               <option value="Republic of Serbia">Republic of Serbia</option>
                               <option value="Reunion">Reunion</option>
                               <option value="Romania">Romania</option>
                               <option value="Russia">Russia</option>
                               <option value="Rwanda">Rwanda</option>
                               <option value="St Barthelemy">St Barthelemy</option>
                               <option value="St Eustatius">St Eustatius</option>
                               <option value="St Helena">St Helena</option>
                               <option value="St Kitts-Nevis">St Kitts-Nevis</option>
                               <option value="St Lucia">St Lucia</option>
                               <option value="St Maarten">St Maarten</option>
                               <option value="St Pierre & Miquelon">St Pierre & Miquelon</option>
                               <option value="St Vincent & Grenadines">St Vincent & Grenadines</option>
                               <option value="Saipan">Saipan</option>
                               <option value="Samoa">Samoa</option>
                               <option value="Samoa American">Samoa American</option>
                               <option value="San Marino">San Marino</option>
                               <option value="Sao Tome & Principe">Sao Tome & Principe</option>
                               <option value="Saudi Arabia">Saudi Arabia</option>
                               <option value="Senegal">Senegal</option>
                               <option value="Seychelles">Seychelles</option>
                               <option value="Sierra Leone">Sierra Leone</option>
                               <option value="Singapore">Singapore</option>
                               <option value="Slovakia">Slovakia</option>
                               <option value="Slovenia">Slovenia</option>
                               <option value="Solomon Islands">Solomon Islands</option>
                               <option value="Somalia">Somalia</option>
                               <option value="South Africa">South Africa</option>
                               <option value="Spain">Spain</option>
                               <option value="Sri Lanka">Sri Lanka</option>
                               <option value="Sudan">Sudan</option>
                               <option value="Suriname">Suriname</option>
                               <option value="Swaziland">Swaziland</option>
                               <option value="Sweden">Sweden</option>
                               <option value="Switzerland">Switzerland</option>
                               <option value="Syria">Syria</option>
                               <option value="Tahiti">Tahiti</option>
                               <option value="Taiwan">Taiwan</option>
                               <option value="Tajikistan">Tajikistan</option>
                               <option value="Tanzania">Tanzania</option>
                               <option value="Thailand">Thailand</option>
                               <option value="Togo">Togo</option>
                               <option value="Tokelau">Tokelau</option>
                               <option value="Tonga">Tonga</option>
                               <option value="Trinidad & Tobago">Trinidad & Tobago</option>
                               <option value="Tunisia">Tunisia</option>
                               <option value="Turkey">Turkey</option>
                               <option value="Turkmenistan">Turkmenistan</option>
                               <option value="Turks & Caicos Is">Turks & Caicos Is</option>
                               <option value="Tuvalu">Tuvalu</option>
                               <option value="Uganda">Uganda</option>
                               <option value="United Kingdom">United Kingdom</option>
                               <option value="Ukraine">Ukraine</option>
                               <option value="United Arab Erimates">United Arab Emirates</option>
                               <option value="Uraguay">Uruguay</option>
                               <option value="Uzbekistan">Uzbekistan</option>
                               <option value="Vanuatu">Vanuatu</option>
                               <option value="Vatican City State">Vatican City State</option>
                               <option value="Venezuela">Venezuela</option>
                               <option value="Vietnam">Vietnam</option>
                               <option value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>
                               <option value="Virgin Islands (USA)">Virgin Islands (USA)</option>
                               <option value="Wake Island">Wake Island</option>
                               <option value="Wallis & Futana Is">Wallis & Futana Is</option>
                               <option value="Yemen">Yemen</option>
                               <option value="Zaire">Zaire</option>
                               <option value="Zambia">Zambia</option>
                               <option value="Zimbabwe">Zimbabwe</option>
                            </select>
                        </div>
                    </div>

                    <div class="col col-sm-2">                     
                        <div class="form-group" id="other_cities" style="display: none;">
                            <input type="text" value="" class="form-control" name="other_cities" placeholder="City">
                        </div>
                    </div>

                    </div>
                    </div>

                </div>
            </div>
            
            <div class="row">
                <div class="tv-post-label">Status</div>
                   
                <div class="col col-sm-8" style="padding: 0px;">
                    
                    <label class="radio-inline" for="tv_status_1"><input type="radio" name="tv_status" id="tv_status_1" value="Studio" onChange="change_tv_status();" />Studio</label>
                    <label class="radio-inline" for="tv_status_2"><input type="radio" name="tv_status" id="tv_status_2" value="Network" onChange="change_tv_status();" />Network</label>
                    <label class="radio-inline" for="tv_status_3"><input type="radio" name="tv_status" id="tv_status_3" value="Independent" onChange="change_tv_status();" />Independent</label>
                    <label  class="radio-inline" for="tv_status_5"><input type="radio" name="tv_status" id="tv_status_5" value="Student" onChange="change_tv_status();" />Student</label>
                </div>
                <div class="col col-sm-4">
                </div> 
            </div>
             <div class="tv-post-row">
                <div class="tv-post-label">Company Name</div>
                <div class="tv-post-input">
                    <div class="tv-radio-row form-group" >
                        <input type="text" class="form-control" name="company_name" id="company_name" value="" />
                    </div>
                </div>
            </div>
            <div class="tv-post-row">
                <div class="tv-post-label">Premier</div>
                <div class="" style="float:left;">
                    <div class="tv-radio-row">
                        <label for="tv_premier_1"><input type="radio" name="tv_premier" id="tv_premier_1" value="yes" /> Yes</label>
                    </div>
                    <div class="tv-radio-row">
                        <label for="tv_premier_2"><input type="radio" name="tv_premier" id="tv_premier_2" value="no" checked /> NO</label>
                    </div>          
                </div>
                <span class="premier_notice" style="display: none;">NYC Crime FilmFest PopUp Festival on April 18 2020</span>
                <select name="year_of_production" id="year_of_production" style="display: none;" >
                    <option value="">Select Year of Production</option>
                                <?php
                                   for ($i=2018; $i<=2020; $i++){
                                     echo "<option value='".$i."'>" . $i ."</option>";
                                   }
                                ?>
                </select>
            </div>

            <div class="episode_season" style="display: none;">
                <div class="tv-post-row cmtv_season_main">
                    <div class="tv-post-label">Season</div>
                    <div class="tv-post-input">
                        <div class="tv-radio-row">
                            <select name="tv_season" id="tv_season" >
                                <option value="">Select Season</option>
                               <option value="2019">2019</option>
                               <option value="2018">2018</option>
                               <option value="2017">2017</option>
                               <option value="2017">2016</option>
                               <option value="2017">2015</option>
                               <option value="2017">2014</option>
                            </select>
                        </div>
                    </div>
                </div>          
                <div class="tv-post-row cmtv_episodes_main">
                    <div class="tv-post-label">Episodes</div>
                    <div class="tv-post-input">
                        <div class="tv-row">
                            <select name="tv_episodes" id="tv_episodes" >
                                <option value="">Select Episodes</option>
                               <option value="2019">2019</option>
                               <option value="2018">2018</option>
                               <option value="2017">2017</option>
                               <option value="2017">2016</option>
                               <option value="2017">2015</option>
                               <option value="2017">2014</option>
                            </select>
                        </div>
                    </div>
                </div>
               <div class="tv-post-row tv-even-row">
                    <div class="tv-post-label">Pilot</div>
                    <div class="tv-post-input">
                        <div class="tv-radio-row form-check">
                            <label class="form-check-label" for="tv_post_type_1_1"><input type="checkbox" class="form-check-input" name="tv_pilot" id="tv_pilot" value="Pilot" />Check as Pilot</label>
                        </div>          
                    </div>  
                </div>

	            

            </div>

            <div class="tv-post-row platform_main">
                <div class="tv-post-label">Platform</div>
                <div class="tv-post-input form-group">
                    <div class="tv-row">
                        <select class="form-control" name="tv_platform" id="tv_platform" >
                            <option value="">Select Platform</option>
                           <option value="Playstation">Playstation</option>
                           <option value="XBox">XBox</option>
                           <option value="PC">PC</option>
                           <option value="Online">Online</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="tv-post-row">
            <h2>User Info</h2>
            </div>
                    <div class="row">
                        
                        <div class="col-sm-3">
                        </div>
                        <div class="col-sm-6">
                          <div class="form-group">
                            <input type="text" value="" class="form-control" name="user_name" id="user_name"   placeholder="<?php _e('User Name', 'kaya_forms'); ?>" required />
                          </div>    
                        </div> 
                        <div class="col-sm-3">
                        </div> 
                    </div>  
                        
                    <div class="row">
                        <div class="col-sm-6">
                          <div class="form-group">
                            <input type="text" value="" class="form-control" name="first_name" id="first_name"   placeholder="<?php _e('First Name', 'kaya_forms'); ?>" required />
                          </div>    
                        </div>  
                        <div class="col-sm-6">
                          <div class="form-group">
                            <input type="text" value="" class="form-control" name="last_name" id="last_name"   placeholder="<?php _e('Last Name', 'kaya_forms'); ?>" required />
                           </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                               <input type="text" value="" class="form-control" name="user_email" id="email" pattern="[^ @]*@[^ @]*"  placeholder="<?php _e('Email', 'kaya_forms'); ?>" required />
                            </div>  
                        </div>  
                        <div class="col-sm-6">                  
                            <div class="form-group">
                                <input type="text" value="" class="form-control" name="confirm_email" id="confirm_email" pattern="[^ @]*@[^ @]*"  placeholder="<?php _e('Confirm Email', 'kaya_forms'); ?>" required />
                            </div>
                      </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-6">
                          <div class="form-group">
                            <input type="password" value="" class="form-control" onchange="form.cpassword.pattern = RegExp.escape(this.value);" name="password" id="password" placeholder="<?php _e('Password', 'kaya_forms'); ?>" required />
                          </div>
                        </div>
                        <div class="col-sm-6">
                         <div class="form-group">
                            <input type="password" value="" class="form-control" name="cpassword" id="cpassword" placeholder="<?php _e('Confirm Password', 'kaya_forms'); ?>" required /></p>
                         </div>
                        </div>
                    </div>
            
            
                    <div class="term_and_cond panel panel-default">
                        <div class="panel-heading">Terms & Condition</div>
                        <div class="panel-body term_cond_body">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </div>
                    </div>
                    <div class="form-group form-check">
                        <label class="form-check-label">
                          <input class="form-check-input" type="checkbox" name="tv_terms_agree"> I have agree to the terms and conditions
                        </label>
                    </div>
                
            
            <div class="tv-post-row">
                <div class="tv-post-label">Payment Method</div>
                <div class="tv-post-input">
                    <div class="tv-radio-row">
                        <label for="tv_payment_method_1"><input type="radio" name="tv_payment_method" id="tv_payment_method_1" value="paypal" checked onChange="change_tv_payment_method();" /> Paypal</label>
                    </div>
                    <div class="tv-radio-row">
                        <label for="tv_payment_method_2"><input type="radio" name="tv_payment_method" id="tv_payment_method_2" value="stripe" onChange="change_tv_payment_method();" /> Stripe</label>
                        
                        <div id="tv_payment_method_2_section" style="display:none;">
                            <div class="tv-radio-sub-row" style="margin-bottom: 10px;">
                                <label for="stripe_credit_card_name">Card Holder Name</label>
                                <input type="text" name="stripe_credit_card_name" id="stripe_credit_card_name" value="" />
                            </div>
                            <div class="tv-radio-sub-row" style="margin-bottom: 10px;">
                                <label for="stripe_credit_card_number">Credit Card Number</label>
                                <input type="text" name="stripe_credit_card_number" id="stripe_credit_card_number" value="" />
                            </div>
                            <div class="tv-radio-sub-row">
                                <div class="tv-radio-col-1">
                                    <label for="stripe_credit_card_exp_month">
                                        Exp Month
                                    </label>
                                    <select name="stripe_credit_card_exp_month" id="stripe_credit_card_exp_month">
                                        <option value="01">01</option>
                                        <option value="02">02</option>
                                        <option value="03">03</option>
                                        <option value="04">04</option>
                                        <option value="05">05</option>
                                        <option value="06">06</option>
                                        <option value="07">07</option>
                                        <option value="08">08</option>
                                        <option value="09">09</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                    </select>                                           
                                </div>
                                <div class="tv-radio-col-2">
                                    <label for="stripe_credit_card_exp_year">
                                        Exp Year
                                    </label>
                                    <select name="stripe_credit_card_exp_year" id="stripe_credit_card_exp_year">
                                    <?php
                                    for( $i = date( 'Y' ); $i < date( 'Y' ) + 25; $i++ ){
                                        echo '<option value="' . $i . '">' . $i . '</option>';  
                                    }
                                    ?>
                                    </select>                                       
                                </div>
                                <div class="tv-radio-col-3">
                                    <label for="stripe_credit_card_cvv">CVV</label>
                                    <input type="text" name="stripe_credit_card_cvv" id="stripe_credit_card_cvv" value="" />
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <?php /*?><div class="tv-radio-row">
                        <label for="tv_payment_method_2"><input type="radio" name="tv_payment_method" id="tv_payment_method_2" value="cash" /> Cash</label>
                    </div>
                    <div class="tv-radio-row">
                        <label for="tv_payment_method_3"><input type="radio" name="tv_payment_method" id="tv_payment_method_3" value="credit_card" /> Credit Card</label>
                    </div>
                    <div class="tv-radio-row">
                        <label for="tv_payment_method_4"><input type="radio" name="tv_payment_method" id="tv_payment_method_4" value="vemo"  /> Vemo</label>
                    </div><?php */?>
                </div>
            </div>
            <div class="tv-post-row">
            <div class="tv-post-label"><div id="html_element"></div></div>
                
            </div>
            
            <div class="row">                   
                <div class="col-sm-4"></div>    
                <div class="col-sm-4" style="text-align: center;">
                    <input type="hidden" name="tv_post_submit" value="1">
                    <button class="sumbit_form" type="submit" button="tv_submit" id="tv_submit">Submit</button>         
                </div>
                <div class="col-sm-4"></div> 
            </div>

        </form>
    </div>  
        <?php
}
    if(is_user_logged_in()){
            ?>
<div class="tv-post-wrapper container">
    <div id="step_2">
        <form action="" id="tv_add_post_form" name="tv_add_post_form" method="post" enctype="multipart/form-data">  
                <div class="tv-post-row" style="text-align:center">
                <h2>Submission Info</h2>
            </div>
            
            
            <div class="tv-post-row">
                <div class="tv-post-label">Post type</div>
                <div class="tv-post-input">
                    <div class="tv-radio-row">
                        <label for="tv_post_type_screen_1"><input type="radio" name="tv_post_type_screen" id="tv_post_type_screen_1" value="Screen" /> Screen</label>
                    </div>
                    <div class="tv-radio-row">
                        <label for="tv_post_type_screen_2"><input type="radio" name="tv_post_type_screen" id="tv_post_type_screen_2" value="Dont Screen" checked /> Don't Screen</label>
                    </div>          
                </div>  
            </div>
            <div class="tv-post-row" id="screen_year_of_production" style="display:none;">
                <div class="tv-post-label">Year of Production</div>
                <div class="tv-post-input">
                    <div class="tv-row">            
                        <select name="screen_year_of_production" id="screen_year_of_production_select">
                            <option value="">Select Year of Production</option>
                                <?php
                                   for ($i=1920; $i<=2020; $i++){
                                     echo "<option value='".$i."'>" . $i ."</option>";
                                   }
                                ?>
                        </select>
                    </div>
                </div>
            </div>

            <!--
            <div class="tv-post-row tv-even-row">
                <div class="tv-post-label">Rating Metric</div>
                <div class="tv-post-input">
                    <div class="tv-radio-row">
                        <label for="tv_rating_metric_1"><input type="radio" name="tv_rating_metric" id="tv_rating_metric_1" value="stars" /> Stars</label>
                    </div>
                    <div class="tv-radio-row">
                        <label for="tv_rating_metric_2"><input type="radio" name="tv_rating_metric" id="tv_rating_metric_2" value="number" /> Numbers</label>
                    </div>
                    <div class="tv-radio-row">
                        <label for="tv_rating_metric_3"><input type="radio" name="tv_rating_metric" id="tv_rating_metric_3" value="percentage" /> Percentages</label>
                    </div>
                    <div class="tv-radio-row">
                        <label for="tv_rating_metric_4"><input type="radio" name="tv_rating_metric" id="tv_rating_metric_4" value="letter" checked /> Letter Grades</label>
                    </div>
                </div>
            </div> 
            -->
        
            
            <div class="tv-post-row featured_video_vimeo_url">
                <div class="row">
                 
                 <div class="col-sm-8">                    
                    <div class="row">
                        <div class="col-sm-4">
                            <h6>Featured Video Url<span style="color:#ff0000;"></span></h6>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label>Youtube/Vimeo Url</label>
                                <input type="text" class="form-control" style="" name="tv_featured_video" id="tv_featured_video" placeholder="Youtube/Vimeo Url" >    
                            </div>
                        </div>
                    </div>
                 </div>

                 <div class="col-sm-4">
                    
                    <div class="row">
                        <div class="col-sm-4">
                            <h6>Featured Video Password<span style="color:#ff0000;"></span></h6>
                        </div>                   
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label>Youtube/Vimeo Password</label>                   
                                <input type="password" class="form-control" style="" name="tv_featured_video_password" id="tv_featured_video_password" >
                            </div>
                        </div>                        
                    </div>

                 </div>

                </div> 
            </div>

            <div class="tv-post-row embed_featured_video" style="display:none;">
                
               
                <h3>Embed Featured Video <span style="color:#ff0000;"></span></h3>
                <div class="form-group">
                    <label for="embed_featured_video">Embed Featured Video</label>
                    <input type="text" class="form-control" name="embed_featured_video" id="embed_featured_video" placeholder="Embed Featured Video">
                </div>

                <div class="form-group">
                    <label for="embed_featured_video_yvlink">Embed Featured Video: Youtube/Vimeo Url</label>
                    <input type="text" style="" class="form-control" name="embed_featured_video_yvlink" id="embed_featured_video_yvlink" placeholder="Embed Youtube/Vimeo Link">                  
                </div>
                <div class="form-group">
                    <label for="featured_video_upload">Featured Video Upload</label>
                    <input type="text" style="" class="form-control" name="featured_video_upload" id="featured_video_upload" placeholder="Featured Video Upload">                  
                </div>
                <div class="form-group">
                    <label for="trailer_video_upload">Trailer Upload</label>
                    <input type="text" style="" class="form-control" name="trailer_video_upload" id="trailer_video_upload" placeholder="Trailer Upload">                  
                </div>

            </div>
            <div class="tv-post-row featured_video_cusom_main" >
                <div class="tv-post-label">Featured Video Upload</div>
                <div class="tv-post-input">
                    <!--<label>(Maximum file size 100 MB, Allowed File types Mp4, AVI, 3GP, FLV)</label> -->
                    
                    <?php //echo do_shortcode('[ajax-file-upload unique_identifier="tv_featured_video_custom" allowed_extensions="mp4,flv,mpeg" on_success_set_input_value="#tv_featured_video_custom" max_size=7000]'); ?>
                    <?php echo do_shortcode('[arfaly id="912"]'); ?>
                                   
                </div>
            </div>
    
             

            <div class="tv-post-row embed_trailer">
                <div class="tv-post-label">Embed Trailer (Vimeo/YouTube)</div>
                <div class="tv-post-input form-group">
                    <label>Embed Trailer</label>
                    <input type="text" style="" name="embed_trailer" class="form-control" id="embed_trailer" placeholder="Embed Trailer (Vimeo/YouTube)" required="">                   
                </div>
            </div>
            <div class="tv-post-row">
                <div class="tv-post-label">Trailer Upload <span style="color:#ff0000;">*</span></div>
                <div class="tv-post-input">
                    <input type="hidden"  value="" />
                    <?php 
                    /* echo do_shortcode('[ajax-file-upload unique_identifier="tv_trailer" allowed_extensions="mp4,flv,mpeg" on_success_set_input_value="#tv_trailer" max_size=7000]'); 
                    <textarea style="display: none;" name="tv_trailer" id="tv_trailer" ></textarea> */ 
                    ?>
                    <?php echo do_shortcode('[arfaly id="929"]'); ?>                               
                </div> 
            </div>
            <div class="tv-post-row tv-even-row">
            
                <div class="tv-post-label">Featured Image <span style="color:#ff0000;">*</span></div>
                <div class="tv-post-input">
                    <?php echo do_shortcode('[arfaly id="927"]'); ?>
                </div> 
            </div>
            <div class="tv-post-row tv-even-row form-group">
                <div class="tv-post-label">Billboard Title</div>
                <div class="tv-post-input">
                    <input type="text" name="tv_billboard_title" class="form-control" id="tv_billboard_title" placeholder="Billboard Title" />                  
                </div>  
            </div>  
            <div class="tv-post-row tv-even-row form-group">
                <div class="tv-post-label">Billboard Subtitle</div>
                <div class="tv-post-input">
                    <input type="text" name="tv_billboard_subtitle" class="form-control" id="tv_billboard_subtitle" placeholder="Billboard Subtitle" />                 
                </div>  
            </div>

            
            <div class="tv-post-row tv-title-row">
                <div class="tv-post-title">Overview</div>   
            </div>
            
            <div class="tv-post-row">
                <div class="tv-post-label">Synopsis <span style="color:#ff0000;">*</span></div>
                <div class="tv-post-input">
                    <textarea name="tv_synopsis" id="tv_synopsis" required></textarea>                  
                </div>  
            </div>
            <div class="tv-post-row tv-even-row">
                <div class="tv-post-label">Features</div>
                <div class="tv-post-input">
                    <textarea name="tv_features" id="tv_features"></textarea>                   
                </div>  
            </div>
            <div class="tv-post-row">
                <div class="tv-post-label">Specifications</div>
                <div class="tv-post-input">
                    <textarea name="tv_specifications" id="tv_specifications"></textarea>                   
                </div>  
            </div>            
            <div class="tv-post-row">
                <div class="tv-post-label">Release date</div>
                <div class="tv-post-input">
                    <input type="date" name="release_date" id="release_date" />                 
                </div>  
            </div>
            
            
            <!--
            <div class="tv-post-row tv-even-row">
                <div class="tv-post-label">Length <span style="color:#ff0000;">*</span></div>
                <div class="tv-post-input">
                    <textarea name="tv_length" id="tv_length" required></textarea>                  
                </div>  
            </div> -->
            <div class="tv-post-row">
                <div class="tv-post-label">Runtime</div>
                <div class="tv-post-input">
              	<div class="tv-row">            
		        <select name="tv_hours" id="tv_hours" >
		        <option value="">Select Hours</option>
			    <?php
			       for ($i=1; $i<=24; $i++){
			         echo "<option value='".$i."'>" . $i ."</option>";
			       }
			    ?>
				</select>
				:
				<select name="tv_minutes" id="tv_minutes" >    
				<option value="">Select MInutes</option>
				<?php

			        for ($i=0; $i<=59; $i++){
			            echo "<option value='".$i."'>" . $i ."</option>";
				      }
				    ?>
				 </select>
		        </div>
		    	</div>
            </div>

            <div class="tv-post-row tv-even-row">
                <div class="tv-post-label">Awards <span style="color:#ff0000;">*</span></div>
                <div class="tv-post-input">
                    <textarea name="tv_awards" id="tv_awards" required></textarea>                  
                </div>  
            </div>
            
            <div class="tv-post-row tv-title-row">
                <div class="tv-post-title">Main Content</div>
                <div class="tv-post-input">             
                </div>              
            </div>
            
            <div id="more_cast_container">
                <div class="tv-post-row tv-even-row">
                    <div class="tv-post-label">Cast</div>
                    <div class="tv-post-input">
                        <div class="tv-radio-row">
                            <select name="tv_cast[]" id="crew">
                            <option  value="Actress" /> Actress</option>
                            <option value="Actor" /> Actor</option>
                            <option value="Stuntman"  /> Stuntman</option>
                            <option value="Stunt Woman"  /> Stunt Woman</option>
                            </select><a class="btn add_more" id="addMore_cast">Add More+</a>
                        </div>          
                    </div>  
                </div> 
                <div class='tv-post-row tv-even-row'>
                    <div class='tv-post-label'>Cast Image</div>
                    <div class='tv-post-input form-group'>
                        <input name='tv_cast_image[]' class="form-control" placeholder='Enter Image url' />
                    </div>
                    <div class='tv-post-row tv-even-row form-group'>
                        <div class='tv-post-label'>Cast Bio</div>
                        <div class='tv-post-input'>
                            <textarea class="form-control" name='tv_cast_bio[]' id=''></textarea>
                        </div>
                    </div>
                </div>          
            </div>          
            <div class="tv-post-row tv-even-row">
                <div class="tv-post-label">Crew</div>
                <div class="tv-post-input">
                    <div class="tv-radio-row">
                    <select name="tv_crew" id="crew">
                        <option value="Production" /> Production</option>
                        <option value="Screen Writer"  /> Screen Writer</option>
                        <option  value="Cinematographer"  /> Cinematographer</option>
                        <option value="Editor"  /> Editor</option>
                        <option value="ExectiveDirector"  />  ExectiveDirector</option>
                    </select><a class="btn add_more" id="addMore_crew">Add More +</a>
                    </div>          
                </div>  
            </div>
            <div id="more_crew_container" class="tv-radio-row form-group">
                <div class='tv-post-row tv-even-row'>
                    <div class='tv-post-label'>Crew Image</div>
                    <div class='tv-post-input'>
                        <input name='tv_crew_image[]' class="form-control" placeholder='Enter Image url' id='' />
                    </div>
                </div>
                <div class='tv-post-row tv-even-row form-group'>
                    <div class='tv-post-label'>Crew Bio</div>
                    <div class='tv-post-input'>
                        <textarea class="form-control" name='tv_crew_bio[]' id='' spellcheck='false'></textarea>
                    </div>
                </div>
            </div>
    

            <div class="tv-post-row tv-even-row">            
                <div class="tv-post-label">New Photo Carosel<span style="color:#ff0000;">*</span></div>
                <div class="tv-post-input">
                   <?php //echo do_shortcode('[arfaly id="1037"]'); ?>
                   <?php //echo do_shortcode('[fu-upload-form]');?>
				   <input type="file" name="upload_attachment[]" class="files" size="50" multiple="multiple" />                
				</div> 
            </div>

            <div class="tv-post-row tv-even-row">
            
                <div class="tv-post-label">Custom Article Editor<span style="color:#ff0000;">*</span></div>
                <div class="tv-post-input">
                    <?php
                        $content = '';
                        $editor_id = 'mycustomeditor';
                        wp_enqueue_media();
                        wp_editor( $content, $editor_id );
                        
                    ?>
                </div> 
            </div>
            
             <div class="tv-post-row">
                <div class="tv-post-label">&nbsp;</div>
                <div class="tv-post-input">
                    <button class="sumbit_form" type="submit"  name="tv_save_post_data" button="tv_save_post_data" id="tv_save_post_data">Save </button>        
                </div>
            </div>  
        </form>
    </div>
</div>
    
        <?php
    }
    ?>
        <script type="text/javascript">
      var onloadCallback = function() {
        grecaptcha.render('html_element', {
          'sitekey' : '6LdwP8UUAAAAAGa9vQ4-mBJW7bEbX3x_NahDivtT'
        });
      };
      function reCaptchaVerify(response) {
    if (response === document.querySelector('#html_element').value) {
        doSubmit = true;
    }
}

function reCaptchaExpired () {
    /* do something when it expires */
}

function reCaptchaCallback () {
    /* this must be in the global scope for google to get access */
    grecaptcha.render('id', {
        'sitekey': RC2KEY,
        'callback': reCaptchaVerify,
        'expired-callback': reCaptchaExpired
    });
}

document.forms['form-name'].addEventListener('submit',function(e){
    if (doSubmit) {
        /* submit form or do something else */
    }
})
    </script>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer>
        
    </script>
    
    <style type="text/css">
       /*.genre_div {
            display: none;
        }  */       
    </style>
    <script type="text/javascript">
    jQuery( document ).ready(function() {
        //console.log( "ready!" );
        
        jQuery("#addMore_crew").click(function() {
         jQuery("#more_crew_container").append("<div class='tv-post-row tv-even-row'><div class='tv-post-label'>Crew Role Name</div><div class='tv-post-input'><input  name='tv_crew[]' placeholder='Enter Crew Role Name'></div></div><div class='tv-post-row tv-even-row'><div class='tv-post-label'>Crew Image</div><div class='tv-post-input'><input name='tv_crew_image[]' placeholder='Enter Image url' id=''/></div></div><div class='tv-post-row tv-even-row'><div class='tv-post-label'>Crew Bio</div><div class='tv-post-input'><textarea name='tv_crew_bio[]' id='' spellcheck='false'></textarea></div></div>");  
        });     
        jQuery("#addMore_cast").click(function() {
         jQuery("#more_cast_container").append("<div class='tv-post-row tv-even-row'><div class='tv-post-label'>Cast Role Name</div><input name='tv_cast[]' placeholder='Cast Name'></div><div class='tv-post-row tv-even-row'><div class='tv-post-label'>Cast Image</div><div class='tv-post-input'><input name='tv_cast_image[]' placeholder='Enter Image url'/></div></div><div class='tv-post-row tv-even-row'><div class='tv-post-label'>Cast Bio</div><div class='tv-post-input'><textarea name='tv_cast_bio[]' id=''></textarea></div></div>");  
        });
        
        jQuery("#us_states").change(function(){
            jQuery("#us_cities").css("display", "inline-block");
        });
        jQuery("#country").change(function(){
            jQuery("#other_cities").css("display", "inline-block");
        });
        
        jQuery( ".crime_selection_label" ).each(function(index) {
            jQuery('.genre_div').hide();
            
            

            jQuery(this).on("click", function(){
                
                 jQuery( ".crime_selection_label" ).each(function(index) {
                    jQuery(".crime_selection_label").removeClass('selected');
                 });   

                console.log( "Clicked...,.,.!" );
                jQuery(this).addClass("selected");


                // For the boolean value
                cat_id = jQuery(this).attr('data-value');
                cat_name = jQuery(this).attr('data-name');      
                
                jQuery(".genre_div").hide();
                //cat_id_dropdwon_id = ;
                
                jQuery('#genre_div'+cat_id).show();
                jQuery('#main_cat'+cat_id).show();

                jQuery('#parent_cat').val(cat_id);
                console.log(cat_name);

                if (cat_name == 'Video Games and New Media'){
                    jQuery('.platform_main').show();
                }

                if(cat_name == 'Network/Cable TV'){
                	console.log("Inside Nwtw Cable TV Cat");
                    jQuery(".episode_season").show();
                }else{
                    jQuery(".episode_season").hide();
                }
                
                
            });
        });

        // //Get uploaded File Video 
        // var featured_video = jQuery("#preview-0").attr('link');
        // console.log(featured_video);
        jQuery('input[name=tv_region]').click(function () {
            console.log("Clicked..");
            console.log(this.id);
            if (this.id == "tv_region_1") {
                jQuery(".united_states_america").css("display", "block");
                jQuery(".radio_international_main").css("display", "none");
                jQuery("#other_cities").css("display", "none");
            } 
            if(this.id == "tv_region_2") {
                jQuery(".united_states_america").css("display", "none");
                jQuery(".radio_international_main").css("display", "block");
                jQuery("#us_cities").css("display", "none");
            }
        });

        //Pilot
        jQuery('input[name=tv_pilot]').click(function () {
            if(jQuery('#tv_pilot').is(':checked')) {
                jQuery(".cmtv_season_main").hide();
                jQuery(".cmtv_episodes_main").hide();
            } 
            else{
                jQuery(".cmtv_season_main").show();
                jQuery(".cmtv_episodes_main").show();
            }
        });

        //Premier
        jQuery('input[name=tv_premier]').click(function () {
            if(this.id == "tv_premier_1") {
                jQuery(".premier_notice").show();
                jQuery("#year_of_production").show();
            } 
            else if(this.id == "tv_premier_2"){
                jQuery(".premier_notice").hide();
                jQuery("#year_of_production").hide();
            }
        });

        //Post Type
        jQuery('input[name=tv_post_type_screen]').click(function () {
            if(this.id == "tv_post_type_screen_1") {
                jQuery(".embed_featured_video").show();
                jQuery(".featured_video_vimeo_url").hide();
                jQuery("#screen_year_of_production").show();
                
            } 
            else if(this.id == "tv_post_type_screen_2"){
                jQuery(".featured_video_vimeo_url").show();
                jQuery("#screen_year_of_production").hide();
                jQuery(".embed_featured_video").hide();
            }
        });
        jQuery('#audience_awards').click(function () {
            var audience_awards_id = jQuery("#audience_awards").attr('data-value');
            //console.log(audience_awards_id);
            
            jQuery("#parent_cat_award").val(audience_awards_id);
            jQuery("#parent_category_award").toggle();
           
            jQuery("#parent_category_award").find('.genre_div').addClass("award_dropdown_sel");
            jQuery("#parent_category_award").find('.genre_div').removeClass("genre_div");

            jQuery("#parent_category_award .genre_div").show();
            jQuery("#parent_category_award .award_dropdown_sel").show();

            jQuery("#parent_category_award .genre_div .award_button.btn a").hide();
            jQuery("form#tv_add_post_form #parent_category_award select.postform").css("display", "block");
            jQuery("form#tv_add_post_form #parent_category_award select.postform").css("position", "inherit");
        });

    });

</script>
    
    
    <?php
    
}
    
    public function crimetvpost_callback() {
        ob_start();
                
        $builders   = it_get_setting( 'front_builder' );

        if( ! empty( $builders ) && count( $builders ) > 2 ) {
            foreach( $builders as $builder ) {
                
                if( ! in_array( $builder['id'], array( 'portholes' ) ) )
                    continue;
                    
                it_shortcode( $builder );           
            }
        }
        
        $this->render_add_post_form();
                        
        if( ! empty( $builders ) && count( $builders ) > 2 ) {
            foreach( $builders as $builder ) {
                
                if( ! in_array( $builder['id'], array( 'explicit', 'headliner' ) ) )
                    continue;
                    
                it_shortcode( $builder );           
            }
        }
            
        return ob_get_clean();
    }
public function crimetvpost_callback_step2() {
        ob_start();
                
        $builders   = it_get_setting( 'front_builder' );

        if( ! empty( $builders ) && count( $builders ) > 2 ) {
            foreach( $builders as $builder ) {
                
                if( ! in_array( $builder['id'], array( 'portholes' ) ) )
                    continue;
                    
                it_shortcode( $builder );           
            }
        }
        
        $this->render_add_post_form_step2();
                        
        if( ! empty( $builders ) && count( $builders ) > 2 ) {
            foreach( $builders as $builder ) {
                
                if( ! in_array( $builder['id'], array( 'explicit', 'headliner' ) ) )
                    continue;
                    
                it_shortcode( $builder );           
            }
        }
            
        return ob_get_clean();
    }

    public function crimetvpost_columns_post_type( $defaults ) {
        $defaults['posttype']   = 'Post Type';
        return $defaults;
    }
    
    public function crimetvpost_columns_post_type_render( $column_name, $post_id ) {
        if ( $column_name == 'posttype' ) {
            $_post_type_screen  = get_post_meta( $post_id, '_tv_post_type_screen', true );
            $_post_type_screen  = strtolower( $_post_type_screen ) == 'screen' ? 'Screen' : "Don't Screen";
            echo $_post_type_screen;        
        }
    }

    public function cutstom_debug() {       
        if( ! isset( $_GET['_tv_debug'] ) )
            return;
        
        $post_id= '823';
        $post   = get_post( $post_id );
        $meta   = get_post_meta( $post_id );    
        
        print_rr( $post );
        print_rr( $meta );
        
        exit;
    }
    
    public function send_mail( $to, $subject, $body, $attachments = array() ) {
        
        // This is headers
        $sender_name    = get_bloginfo( 'name' );
        
        $domain         = $this->get_domain_name();
        
        $sender_email   = "mail@" . $domain;
        $return_email   = "mail@" . $domain;
        
        $subject_preferences    = array(
                                    'scheme'            => 'Q',
                                    'input-charset'     => 'UTF-8',
                                    'output-charset'    => 'UTF-8',
                                    'line-length'       => 76,
                                    'line-break-chars'  => "\r\n",
                                    );
                    
        $headers     = "From: " . $sender_name . " <" . $sender_email . ">" . "\r\n";
        $headers    .= "Reply-To: " . $return_email . "" . "\r\n";
        $headers    .= "Return-Path: " . $return_email . "\r\n";
        $headers    .= "MIME-Version: 1.0\r\n";
        $headers    .= "Content-type: text/plain; charset: utf8\r\n";
        $headers    .= "X-Mailer: PHP/" . phpversion() . "\r\n";
        $headers    .= "X-Priority: 1 (Highest)\n";
        $headers    .= "X-MSMail-Priority: High\n";
        $headers    .= "Importance: High\n";
        
        if( $_SERVER['SERVER_ADDR'] ) {
            $headers    .= "X-Originating-IP: " . $_SERVER['SERVER_ADDR'] . "" . "\r\n";    
        }
        if( function_exists( 'iconv_mime_encode' ) ) {
            $headers    .= iconv_mime_encode( "Subject", $subject, $subject_preferences ) . "\n";
        }
        
        // If you want to attach a file to mail, Please give file path (Not url), for eg. /home/html/uploads/file.jpg
        $attachments    = array();
        
        //$this->log( $headers );
        //$this->log( array( $to, $subject, $body ) );
        
        return wp_mail( $to, $subject, $body, $headers, $attachments ); 
    }
    
    public function get_domain_name() {
        
        $domain = site_url( "/" ); 
        $domain = str_replace( array( 'http://', 'https://', 'www' ), '', $domain );
        $domain = explode( "/", $domain );
        $domain = $domain[0] ? $domain[0] : $_SERVER['SERVER_ADDR'];    
        
        return $domain;
    }
    
    public function get_log_directory() {
        $wp_upload_dir  = wp_upload_dir();
        $upload_dir     = $wp_upload_dir['basedir']; 
        $log_directory  = join( DIRECTORY_SEPARATOR, array( $upload_dir, 'crime-tv-post', 'log' ) );  
        
        wp_mkdir_p( $log_directory );   // Create dir if not exists
        
        return $log_directory; 
    }
    
    public function log( $msg = "" ) {
        
        if( ! $this->enable_log )
            return;
                        
        $logfile= $this->get_log_directory() . "/debug.log";
        
        $msg    = ( is_array( $msg ) || is_object( $msg ) ) ? print_r( $msg, 1 ) : $msg;
            
        error_log( date('[Y-m-d H:i:s e] ') . $msg . PHP_EOL, 3, $logfile );
    }
}

endif;

global $crime_tv_post; 
$crime_tv_post  = new Crime_TV_Post();

if ( ! function_exists( 'it_get_author_avatar2' ) ) {
    #html display of author avatar
    function it_get_author_avatar2( $size, $user_number = 1 ) {
        
        $post_id        = get_the_ID();
        $user_image_1   = get_post_meta( $post_id, '_tv_user_image_1_id', true );
            
        if( $user_image_1 ) {
            $user_image = wp_get_attachment_image_src( $user_image_1, array( $size, $size ) );
            $user_image = $user_image ? $user_image[0] : false;
        }
        
        $out = '';
        $out .= '<div class="author-image thumbnail">';
            $out .= '<a class="info" title="'.__('See all posts from this author',IT_TEXTDOMAIN).'" href="'.get_author_posts_url(get_the_author_meta('ID')).'">';
                if( $user_image ) {
                    $out .= '<img alt="" src="' . $user_image . '" srcset="' . $user_image . ' 2x" class="avatar avatar-40 photo avatar-default img-circle" width="40" height="40">';
                } else {
                    $out .= get_avatar(get_the_author_meta('user_email'), $size);
                }
            $out .= '</a>';
        $out .= '</div>';
        
        return $out;        
    }
}

function install_crimetv_payment() {
    
    global $wpdb;
    
    $table_name         = $wpdb->prefix . 'crimetv_payment';
    
    $charset_collate    = $wpdb->get_charset_collate();
    print_r($wp_user_id);
    //$wp_user_id = "46";
    
    $sql = "CREATE TABLE IF NOT EXISTS " . $table_name . " (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        wp_user_id mediumint(9) NOT NULL,
        post_id mediumint(9) NOT NULL,
        amount float(10,2) NOT NULL,
        payment_method varchar(20) NOT NULL,
        transaction_id varchar(255) NOT NULL,
        payment_status varchar(100) NOT NULL,
        updated_at datetime NOT NULL,
        created_at datetime NOT NULL,
        ip varchar(50) NOT NULL,
        PRIMARY KEY  (id)
    ) $charset_collate;";
    
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
}
register_activation_hook( __FILE__, 'install_crimetv_payment' );


// uninstall 
function uninstall_crimetv_payment(){
    global $wpdb;
    
    $table_name = $wpdb->prefix . 'crimetv_payment';
    $sql        = "DROP TABLE IF EXISTS $table_name";
    $wpdb->query( $sql );
}
register_uninstall_hook( __FILE__, 'uninstall_crimetv_payment' );