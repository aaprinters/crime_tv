<?php
// var functions
if( ! function_exists('print_rr') ) {
	function print_rr( $content ) {
		echo "<pre>";
		print_r( $content );
		echo "</pre>";	
	}
} 

if ( ! function_exists( 'siget' ) ) {
	function siget( $name, $array = null ) {
		if ( ! isset( $array ) ) {
			$array = $_GET;
		}

		if ( isset( $array[ $name ] ) ) {
			return $array[ $name ];
		}

		return '';
	}
}

if ( ! function_exists( 'sipost' ) ) {
	function sipost( $name, $do_stripslashes = true ) {
		if ( isset( $_POST[ $name ] ) ) {
			return $do_stripslashes && function_exists( 'stripslashes_deep' ) ? stripslashes_deep( $_POST[ $name ] ) : $_POST[ $name ];
		}

		return '';
	}
}

if ( ! function_exists( 'siar' ) ) {
	function siar( $array, $name ) {
		if ( isset( $array[ $name ] ) ) {
			return $array[ $name ];
		}

		return '';
	}
}

if ( ! function_exists( 'siars' ) ) {
	function siars( $array, $name ) {
		$names = explode( '/', $name );
		$val   = $array;
		foreach ( $names as $current_name ) {
			$val = siar( $val, $current_name );
		}

		return $val;
	}
}

if ( ! function_exists( 'siempty' ) ) {
	function siempty( $name, $array = null ) {

		if ( is_array( $name ) ) {
			return empty( $name );
		}

		if ( ! $array ) {
			$array = $_POST;
		}

		$val = siar( $array, $name );

		return empty( $val );
	}
}

if ( ! function_exists( 'siblank' ) ) {
	function siblank( $text ) {
		return empty( $text ) && strval( $text ) != '0';
	}
}

if ( ! function_exists( 'siobj' ) ) {
	function siobj( $obj, $name ) {
		if ( isset( $obj->$name ) ) {
			return $obj->$name;
		}

		return '';
	}
}
if ( ! function_exists( 'siexplode' ) ) {
	function siexplode( $sep, $string, $count ) {
		$ary = explode( $sep, $string );
		while ( count( $ary ) < $count ) {
			$ary[] = '';
		}

		return $ary;
	}
}

if ( ! function_exists( 'number_formats' ) ) {
	function number_formats( $value = "" ) {		
		if ( $value == "" ) {
			$value 	= 0;
		} 	
		return number_format( $value, 2, '.', ',' );		
	}
}

if ( ! function_exists( 'generate_random_string' ) ) {
	function generate_random_string( $length = 10 ) {
    	$characters 		= '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    	$charactersLength 	= strlen( $characters );
    	$randomString 		= '';
		
	    for ( $i = 0; $i < $length; $i++ ) {
    		$randomString  .= $characters[ rand( 0, $charactersLength - 1 ) ];
	    }
		
    	return $randomString;
	}
}

//  Get Current IP Address
if ( ! function_exists( 'get_ip' ) ) {
	function get_ip() {
		if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {   			//check ip from share internet	
		  	$ip	= $_SERVER['HTTP_CLIENT_IP'];
		} elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) { 	//to check ip is pass from proxy		
		  	$ip	= $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
		  	$ip	= $_SERVER['REMOTE_ADDR'];
		}
		
		return $ip;
	}
}

if ( ! function_exists( 'format_size_units' ) ) {
	 function format_size_units( $bytes ) {
		if ( $bytes >= 1073741824 ) {
			$bytes = number_format( $bytes / 1073741824, 2 ) . ' GB';
		} elseif ( $bytes >= 1048576 ) {
			$bytes = number_format( $bytes / 1048576, 2 ) . ' MB';
		} elseif ( $bytes >= 1024 ) {
			$bytes = number_format( $bytes / 1024, 2 ) . ' KB';
		} elseif ( $bytes > 1 ) {
			$bytes = $bytes . ' bytes';
		} elseif ( $bytes == 1 ) {
			$bytes = $bytes . ' byte';
		} else {
			$bytes = '0 bytes';
		}

		return $bytes;
	}
}

if ( ! function_exists( 'si_array_map_recursive' ) ) {
	function si_array_map_recursive( $callback, $array ) {
		foreach( $array as $key => $value ) {
			if( is_array( $array[ $key ] ) ) {
				$array[ $key ] = si_array_map_recursive( $callback, $array[ $key ] );
			} else {
				$array[ $key ] = call_user_func( $callback, $array[ $key ] );
			}
		}
		
		return $array;
	}
}	

if ( ! function_exists( 'base64_url_encode' ) ) {
 	function base64_url_encode( $data ) { 
		return rtrim( strtr( base64_encode( $data ), '+/', '-_' ), '=' ); 
	}
}

if ( ! function_exists( 'base64_url_decode' ) ) {	
	function base64_url_decode( $data ) { 
		return base64_decode( str_pad( strtr( $data, '-_', '+/' ), strlen( $data ) % 4, '=', STR_PAD_RIGHT ) ); 
	}
}