function change_tv_payment_method() {
	if( jQuery("input[name=tv_payment_method]:checked").val() == 'stripe' ) {
		jQuery("#tv_payment_method_2_section").show();
	} else {
		jQuery("#tv_payment_method_2_section").hide();
	}
}

function change_tv_status() {
	if( jQuery("input[name=tv_status]:checked").val() == 'Studio' || jQuery("input[name=tv_status]:checked").val() == 'Independant' ) {
		jQuery("#tv-custom-fest-categories-section").show();
		jQuery("#tv-standard-categories-section").show();
	} else {
		jQuery("#tv-custom-fest-categories-section").hide();
		jQuery("#tv-standard-categories-section").hide();
	}
	
	if( jQuery("input[name=tv_status]:checked").val() == 'Youtube' ) {
		jQuery("#tv-youtube-categories-section").show();
	} else {
		jQuery("#tv-youtube-categories-section").hide();
	}
}