<?php

if( ! class_exists('WP_List_Table') ){
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

if( ! class_exists( 'Shipping_Rates_List_Page' ) ) :

class CrimeTV_List_Page extends WP_List_Table {

    var $_data = array();

	function __construct(){

        global $status, $page;
		
        //Set parent defaults
        parent::__construct( array(
            'singular'  => 'Payment',     	//singular name of the listed records
            'plural'    => 'Payments',    	//plural name of the listed records
            'ajax'      => true       		//does this table support ajax?
        ) );		
    }

	function column_default( $item, $column_name ){
        switch( $column_name ){
            default:
                return isset( $item[ $column_name ] ) ? $item[ $column_name ] : '';	//print_r( $item, true ) //Show the whole array for troubleshooting purposes
        }
    }
	
    function column_cb( $item ){

        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            /*$1%s*/ $this->_args['singular'],  //Let's simply repurpose the table's singular label ("movie")
            /*$2%s*/ $item['ID']                //The value of the checkbox should be the record's id
        );
    }

    function get_columns(){

        $columns = array(
            'cb'        		=> '<input type="checkbox" />', //Render a checkbox instead of text
            'ID'				=> 'ID',	
			'wp_user_id'		=> 'WP User',	
			'post_id'			=> 'Post ID',	
			'payment_method'	=> 'Payment Method',				
			'amount'			=> 'Amount',
			'payment_status'	=> 'Payment Status',
			'transaction_id'	=> 'Transaction ID',
			'created_at'		=> 'Date/Time',		
        );

        return $columns;
    }

    function get_sortable_columns() {
		
        $sortable_columns = array(
            //'name'	=> array( 'name', true ),     //true means it's already sorted
            //'url'  	=> array('url', false)
        );

        return $sortable_columns;
    }

    function get_bulk_actions() {

        $actions = array(
        	//'delete'    => 'Delete'
        );
		
        return $actions;
    }
	
    function process_bulk_action() {

        //Detect when a bulk action is being triggered...

        if( 'delete' === $this->current_action() ) {

			global $wpdb;

			$items	= $_GET['payment'];

			if( is_array( $items ) && $items ) {
				foreach( $items as $single_item ) {					
					$wpdb->delete( $wpdb->prefix . "crimetv_payment", array( 'ID' => absint( $single_item ) ) );
				}

				global $is_item_deleted;
				$is_item_deleted	= true;	
			}		

			//wp_die('Items deleted (or they would be if we had items to delete)!');
        }        
    }
	
	function count_total_items() {
		global $wpdb;
				
		return $wpdb->get_var( "SELECT COUNT(*) FROM " . $wpdb->prefix . "crimetv_payment" );
	}
	
    function prepare_items() {

        global $wpdb; //This is used only if making any database queries

		// Get Per Page

        $user_ID 	= get_current_user_id();

		$screen 	= get_current_screen();

		$option 	= $screen->get_option( 'per_page', 'option' );

		$per_page 	= get_user_meta( $user_ID, $option, true );

		if ( empty ( $per_page ) || $per_page < 1 ) {
    		$per_page	= $screen->get_option( 'per_page', 'default' );
		}		

		$current_page 	= $this->get_pagenum();
		
		// Get Columns
        $columns 	= $this->get_columns();

        $hidden 	= array();

        $sortable 	= $this->get_sortable_columns();

        $this->_column_headers = array( $columns, $hidden, $sortable );

        $this->process_bulk_action();        

		// For search
		$search = ( isset( $_REQUEST['s'] ) ) ? $_REQUEST['s'] : false;
			
		$payment_methods	= array(
								'paypal'		=> 'Paypal',
								'cash' 			=> 'Cash',
								'credit_card'	=> 'Credit Card',
								'vemo'			=> 'Vemo',
								);
					
        $data	= array();
	
		$all_payments	= $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "crimetv_payment ORDER BY id DESC LIMIT " . $per_page . " OFFSET " . ( ( $current_page - 1 ) * $per_page ) );

		if( $all_payments ) {
			foreach( $all_payments as $single_payment ) {
				
				$user_object	= get_userdata( $single_payment->wp_user_id );
				if ( ! is_wp_error( $user_object ) && current_user_can( 'edit_user', $user_object->ID ) ) {
					$edit_link 	= esc_url( add_query_arg( 'wp_http_referer', urlencode( wp_unslash( $_SERVER['REQUEST_URI'] ) ), get_edit_user_link( $user_object->ID ) ) );
					$user_link 	= "<strong><a target=\"_blank\" href=\"{$edit_link}\">{$user_object->user_login}</a></strong>";
				} else if ( ! is_wp_error( $user_object ) && ! current_user_can( 'edit_user', $user_object->ID ) ) {
					$user_link 	= "<strong>{$user_object->user_login}</a>";
				} else {
					$user_link 	= "<strong>{$single_payment->wp_user_id}</strong>";
				}
				
				$data[]	= array( 
								'ID'				=> $single_payment->id,
								'payment_method'	=> isset( $payment_methods[ $single_payment->payment_method ] ) ? $payment_methods[ $single_payment->payment_method ] : $single_payment->payment_method,
								'wp_user_id'		=> $user_link,
								'post_id' 			=> '<strong><a href="' . get_edit_post_link( $single_payment->post_id ) . '" target="_blank">' . $single_payment->post_id . '</a></strong>',
								'amount' 			=> $single_payment->amount,
								'transaction_id' 	=> $single_payment->transaction_id,
								'payment_status' 	=> $single_payment->payment_status,
								'created_at' 		=> $single_payment->created_at,
								);
			}
		}
		
        $total_items 	= $this->count_total_items();
		
        $this->items 	= $data;

		$this->set_pagination_args( array(
            'total_items' => $total_items,                  	//WE have to calculate the total number of items
            'per_page'    => $per_page,                     	//WE have to determine how many items to show on a page
            'total_pages' => ceil( $total_items / $per_page )   //WE have to calculate the total number of pages
        ) );
    }
}

endif;