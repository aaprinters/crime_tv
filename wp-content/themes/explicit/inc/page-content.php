<?php
#default settings (use "Standard Pages" theme options for defaults)
$sidebar = __('Page Sidebar',IT_TEXTDOMAIN);
$layout = 'classic';
$thumbnail = it_get_setting('page_featured_image_size');
$sidebar_position = it_get_setting('page_sidebar_position');
$post_article_title_disable = it_get_setting('post_article_title_disable');
$editor_rating_disable = it_get_setting('review_editor_rating_disable');
$user_rating_disable = it_get_setting('review_user_rating_disable');
$clickable = !it_get_setting('clickable_image_disable');
$disable_view_count = it_component_disabled('views', $post->ID);
$disable_like_count = it_component_disabled('likes', $post->ID);
$disable_comment_count = it_component_disabled('comment_count', $post->ID);
$disable_awards = it_component_disabled('awards', $post->ID);
$disable_authorship = it_component_disabled('authorship', $post->ID);
$disable_comments = it_component_disabled('comments', $post->ID);
$disable_postnav = it_component_disabled('postnav', $post->ID);
$disable_sharing = it_component_disabled('sharing', $post->ID);
$disable_controlbar = it_component_disabled('controlbar', $post->ID, $forcepage = true);
$disable_video = it_component_disabled('video', $post->ID);
$caption = it_get_setting('featured_image_caption');
$disable_authorship_avatar = it_get_setting('post_authorship_avatar_disable');
$sharing_position = it_get_setting('sharing_position');
$template = it_get_template_file();
$details_position = 'none';
$ratings_position = 'none';
$reactions_position = 'none';
$contents_menu = 'none';
$affiliate_position = 'before-content';
$disable_postinfo = true;
$image_can_float = false;
$disabled_menu_items = array();
$article_title = '';
$post_article_title = false;
$disable_main_header = false;
$disable_recommended = false;
$disable_title = false;
$isreview = false;
$full = '';
$pagecss = '';
$item_type = 'http://schema.org/Article';
$item_reviewed = '';
$csscol1 = '';
$csscol2 = '';
$csscol3 = '';
$has_details = it_has_details($post->ID);
$has_rating = it_has_rating($post->ID);

#get just the primary category id
$categoryargs = array('postid' => $post->ID, 'label' => false, 'icon' => false, 'white' => true, 'single' => true, 'wrapper' => false, 'id' => true);	
$category_id = it_get_primary_categories($categoryargs);
$categorycss = ' category-' . $category_id;
#reset args for category display
$categoryargs = array('postid' => $post->ID, 'label' => true, 'icon' => true, 'white' => true, 'single' => true, 'wrapper' => false, 'id' => false);
$category = it_get_primary_categories($categoryargs);

#section-specific settings
if(is_404()) {
	wp_reset_postdata();
	#settings for 404 pages
	$content_title = __('404 Error - Page Not Found', IT_TEXTDOMAIN);
	$title = __('We could not find the page you were looking for. Try searching for it:', IT_TEXTDOMAIN);		
	$disable_controlbar = true;	
	$disable_main_header = true;
	$disable_title = true;
	$disable_recommended = true;
	$disable_sharing = true;
	$disable_authorship = true;
	$layout = 'classic';
} elseif(is_page()) {
	#settings for all standard WordPress pages	
	$title = get_post_meta($post->ID, "_subtitle", $single = true);	
	$page_comments = it_get_setting('page_comments');
	$thumbnail_specific = it_get_setting('page_featured_image_size');	
	$disable_recommended = true;
	$disable_authorship = true;
	$disabled_menu_items[] = 'rating';
	$disabled_menu_items[] = 'overview';
	$layout = 'classic';
	$disable_postnav = true;
	if(!$page_comments) {
		$disable_comments = true;
		$disable_comment_count = true;
		$disabled_menu_items[] = 'comments';
	}
} elseif(is_single()) {
	#settings for single posts
	$layout = it_get_setting('post_layout');
	$subtitle = get_post_meta($post->ID, "_subtitle", $single = true);	
	$billboard_title = get_post_meta($post->ID, "_billboard_title", $single = true);	
	$sidebar_position_specific = it_get_setting('post_sidebar_position');
	$thumbnail_specific = it_get_setting('post_featured_image_size');	
	$contents_menu = it_get_setting('contents_menu');	
	$article_title = it_get_setting('article_title');	
	$title = $category;
	$disable_postinfo = false;
	$details_position = it_get_setting('review_details_position');
	$details_position = !empty($details_position) ? $details_position : 'top';
	$ratings_position = it_get_setting('review_ratings_position');
	$ratings_position = !empty($ratings_position) ? $ratings_position : 'top';	
	$reactions_position = it_get_setting('reactions_position');
	$reactions_position = !empty($reactions_position) ? $reactions_position : 'bottom';	
	if(!comments_open()) $disabled_menu_items[] = 'comments';
	$affiliate_position = it_get_setting('affiliate_position');	
	$affiliate_position = !empty($affiliate_position) ? $affiliate_position : 'after-content';	
}
#settings for buddypress pages
if(it_buddypress_page()) {	
	$disable_postnav = true;
	$disable_controlbar = true;
	$disable_recommended = true;
	$disable_postinfo = true;
	$disable_authorship = true;
	$layout = 'classic';
	$pagecss = 'bp-page';
	$article_title = '';
	$contents_menu = 'none';
	$reactions_position = 'none';
	$sidebar_position_specific = it_get_setting('bp_layout');
	if(it_get_setting('bp_sidebar_unique')) $sidebar = __('BuddyPress Sidebar',IT_TEXTDOMAIN);	
}
#settings for woocommerce pages
if(it_woocommerce_page()) {	
	$disable_postnav = true;
	$disable_controlbar = true;
	$disable_recommended = true;
	$disable_postinfo = true;
	$disable_authorship = true;
	$layout = 'classic';
	$pagecss = 'woo-page';
	$article_title = '';
	$contents_menu = 'none';
	$reactions_position = 'none';
	$sidebar_position_specific = it_get_setting('woo_layout');
	if(it_get_setting('woo_sidebar_unique')) $sidebar = __('WooCommerce Sidebar',IT_TEXTDOMAIN);	
}
#specific template files
switch($template) {
	case 'template-authors.php':
		$pagecss = 'template-authors';		
		$disable_controlbar = true;	
		$disable_main_header = true;
		$disable_recommended = true;	
		$disable_title = true;	
		$layout = 'classic';	
	break;	
}

#don't use specific settings if they are not set
if(!empty($sidebar_position_specific) && $sidebar_position_specific!='') $sidebar_position = $sidebar_position_specific;
if(!empty($thumbnail_specific) && $thumbnail_specific!='') $thumbnail = $thumbnail_specific;

#page-specific settings
$sidebar_position_meta = get_post_meta($post->ID, "_sidebar_position", $single = true);
if(!empty($sidebar_position_meta) && $sidebar_position_meta!='') $sidebar_position = $sidebar_position_meta;
$layout_meta = get_post_meta($post->ID, "_post_layout", $single = true);
if(!empty($layout_meta) && $layout_meta!='' && !is_404()) $layout = $layout_meta;
$thumbnail_meta = get_post_meta($post->ID, "_featured_image_size", $single = true);
if(!empty($thumbnail_meta) && $thumbnail_meta!='') $thumbnail = $thumbnail_meta;
$sidebar_meta = get_post_meta($post->ID, "_custom_sidebar", $single = true);
if(!empty($sidebar_meta) && $sidebar_meta!='') $sidebar = $sidebar_meta;
$post_type = get_post_meta( $post->ID, IT_META_POST_TYPE, $single = true );
$disable_title_meta = get_post_meta($post->ID, IT_META_DISABLE_TITLE, $single = true);
if(!empty($disable_title_meta) && $disable_title_meta!='') $disable_title = $disable_title_meta;
$article_title_meta = get_post_meta($post->ID, "_article_title", $single = true);
if(!empty($article_title_meta) && $article_title_meta!='') $article_title = $article_title_meta;
$disable_review = get_post_meta($post->ID, IT_META_DISABLE_REVIEW, $single = true);
$video = get_post_meta($post->ID, "_featured_video", $single = true);
$sharing_disable_meta = get_post_meta($post->ID, "_sharing_disable", $single = true);
if(!empty($sharing_disable_meta) && $sharing_disable_meta!='') $disable_sharing = $sharing_disable_meta;
$view_count_disable_meta = get_post_meta($post->ID, "_view_count_disable", $single = true);
if(!empty($view_count_disable_meta) && $view_count_disable_meta!='') $disable_view_count = $view_count_disable_meta;
$like_count_disable_meta = get_post_meta($post->ID, "_like_count_disable", $single = true);
if(!empty($like_count_disable_meta) && $like_count_disable_meta!='') $disable_like_count = $like_count_disable_meta;

#contents menu
$contents_menu_display = false;
$contents_menu_meta = get_post_meta($post->ID, "_contents_menu", $single = true);
if($contents_menu=='optin' && $contents_menu_meta) $contents_menu_display = true;
if(($contents_menu=='both' || ($contents_menu=='reviews' && $disable_review!='true')) && !$contents_menu_meta) $contents_menu_display = true;
if($details_position=='none') $disabled_menu_items[] = 'overview';

#this post is a review
if($post_type=='review' && !$editor_rating_disable) {
	#rich snippets
	$item_type = 'http://schema.org/Review';
	$isreview = true;
} elseif($user_rating_disable) {
	$disabled_menu_items[] = 'rating';
	$ratings_position = 'none';
}
if(($post_type=='article' || $disable_review=='true') && $post_article_title_disable) $article_title = '';

if(empty($sidebar_position)) $sidebar_position = 'sidebar-right';
#determine css classes
switch($sidebar_position) {
	case 'sidebar-right':
		$csscol2 = 'col-md-8';
		$csscol3 = 'col-md-4';
	break;
	case 'sidebar-left':
		$csscol1 = 'col-md-4';
		$csscol2 = 'col-md-8';
	break;
	case 'full':
		$csscol2 = 'col-md-12';
	break;	
}

#special class for full-width featured images
if($thumbnail == '790') $imagecss = ' full-image';

#full width layout needs large full width featured image
if($sidebar_position == 'full' && $thumbnail == '790') $thumbnail = '1130';

$disable_subtitle = empty($title) ? true : false;
$csspostnav = $disable_subtitle ? ' moved' : '';
$cssadminbar = is_admin_bar_showing() ? ' admin-bar' : '';
$cssboxed = $layout=='billboard' ? ' boxed' : '';

#determine featured image floating
if(!$contents_menu_display && (!$has_details || $details_position!='top') && ($ratings_position!='top' || $post_type=='article' || $disable_review=='true' || ($ratings_position=='top' && !$has_rating)) && $reactions_position!='top' && $thumbnail_meta!='none' && $thumbnail_meta!='790') $image_can_float = true;
$imagecss = $image_can_float ? ' floated-image' : '';

#get largest size featured images for overlay backgrounds
$overlay_image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
$bg_image = get_post_meta($post->ID, "_bg_image", $single = true);
$billboard_overlay = !empty($bg_image) ? $bg_image : $overlay_image[0];

#get sharing position:
$sharing_position = $disable_sharing ? 'none' : $sharing_position;

#setup args
$awardsargs = array('postid' => $post->ID, 'single' => true, 'badge' => false, 'white' => false, 'wrapper' => true);
$likesargs = array('postid' => $post->ID, 'label' => false, 'icon' => true, 'clickable' => true, 'tooltip_hide' => false);
$viewsargs = array('postid' => $post->ID, 'label' => false, 'icon' => true, 'tooltip_hide' => false);
$commentsargs = array('postid' => $post->ID, 'label' => false, 'icon' => true, 'showifempty' => true, 'tooltip_hide' => false, 'anchor_link' => true);
$imageargs = array('postid' => $post->ID, 'size' => 'single-'.$thumbnail, 'width' => 818, 'height' => 450, 'wrapper' => true, 'itemprop' => true, 'link' => $clickable, 'type' => 'normal', 'caption' => $caption);
$videoargs = array('url' => $video, 'video_controls' => 'true', 'parse' => true, 'frame' => true, 'autoplay' => 0, 'type' => 'embed');
$sharingargs = array('title' => get_the_title(), 'description' => it_excerpt(500, false), 'url' => get_permalink(), 'showmore' => true, 'style' => 'single');

?>

<?php do_action('it_before_content_page'); ?>

<?php if($layout=='billboard') { ?>

	<div class="billboard-image" style="background-image:url(<?php echo $billboard_overlay; ?>);"></div>

	<div class="billboard-overlay"></div>

	<div class="container nobg <?php echo $layout ?>">
    
    	<div class="row">
            
			<?php do_action('it_before_billboard_title', it_get_setting('ad_billboard_before'), 'before-billboard'); ?>
            
         
            <?php			
			
			/* 
			
			   <h1 class="main-title">								
                <?php echo get_the_title();?>                                    
            </h1> 
			Remove subtitle section
			if(!$disable_authorship_avatar) echo '<div class="billboard-avatar">' . ( get_post_type( get_the_ID() ) == 'post' ? it_get_author_avatar2(40) : it_get_author_avatar(40) ) . '</div>'; ?>
            
            <?php if(!empty($subtitle)) echo '<div class="billboard-subtitle">' . $subtitle . '</div>';  */?>           
            
            <?php if(!$disable_authorship) echo '<div class="billboard-authorship">' . it_get_authorship('both', true) . '</div>'; ?>
            
            <?php do_action('it_after_billboard_title', it_get_setting('ad_billboard_after'), 'after-billboard'); ?>	

<?php } ?>
<div class="row">
	<div class="col-md-12 post_title_main">
		<?php //echo '<h3 class="billboard_title">'.$billboard_title.'</h3>'; 
		echo '<h1 class="billboard_title">'.get_the_title().'</h1>'; 
		//echo '<h5 class="sub_title">'.$subtitle.'</h5>'; ?>
	</div>
</div>

<div class="container<?php echo $cssboxed ?> crime_tv_post">		
		<div id="page-content" class="single-page <?php echo $pagecss . ' ' . $sidebar_position . $imagecss . $categorycss ?>" data-location="single-page" data-postid="<?php echo $post->ID; ?>">
    
    	<?php if(!$disable_main_header) { ?>
        
        	<?php do_action('it_before_controlbar', it_get_setting('ad_controlbar_before'), 'before-controlbar'); ?>
        
            
			<div class="row main-header">

            	

                <div class="col-md-12">
				
				<?php 
				 
				 if(!$disable_controlbar) { ?>
                    
                        <div class="bar-header full-width clearfix page-controls">
                        
                            <?php if(!$disable_subtitle) { ?>
                    
                                <div class="bar-label-wrapper">
                                
                                    <div class="bar-label">
                                    
                                        <div class="label-text">
                                        
                                            <?php echo $title; ?>
                                            
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                            
                            <?php } ?>
                            
                            <div class="bar-controls<?php echo $csspostnav; ?>">
                                
                                <?php if(!$disable_like_count) echo '<div class="control-box">' . it_get_likes($likesargs) . '</div>'; ?>
                                
                                <?php if(!$disable_view_count) echo '<div class="control-box no-link">' . it_get_views($viewsargs) . '</div>'; ?>
                                
                                <?php if(!$disable_comment_count) echo '<div class="control-box">' . it_get_comments($commentsargs) . '</div>'; ?>
                                
                                <?php if(!$disable_awards) echo it_get_awards($awardsargs); ?>
                                
                                <?php if(!$disable_postnav) echo '<div class="control-box">' . it_get_postnav() . '</div>'; ?>
                                
                                <?php if($sharing_position=='control_bar') echo '<div class="share-wrapper' . $cssadminbar . '">' . it_get_sharing($sharingargs) . '</div>'; ?>
                                
                            </div>
                            
                        </div>
                        
                    <?php } ?>
                    
                    <?php if($layout!='billboard') { ?>
                    
                        <?php do_action('it_before_standard_title', it_get_setting('ad_title_before'), 'before-title'); ?>
                                        
                        <?php if(!$disable_title) { ?>
                        
                            <h1 class="main-title">	
                            							
                                <?php echo $content_title = empty($content_title) ? get_the_title() : $content_title; ?>  
                                                                  
                            </h1>
                            
                        <?php } ?>
                        
                        <?php if(!$disable_authorship) echo it_get_authorship('both', true); ?>
                        
                        <?php do_action('it_after_standard_title', it_get_setting('ad_title_after'), 'after-title'); ?>
                        
                    <?php } ?>
            
                </div>
            
            </div>
            
            <?php do_action('it_after_controlbar', it_get_setting('ad_controlbar_after'), 'after-controlbar'); ?>
            
        <?php } ?>
			
        <div class="row">
        
        	<?php if($sidebar_position=='sidebar-left') { ?>
            
            	<div class="<?php echo $csscol1; ?>">
            
               		<?php echo it_widget_panel($sidebar, $sidebar_position); ?>
                    
                </div>
                            
            <?php } ?>
            
            <div id="main-content" class="<?php echo $csscol2; ?>">
                              
				<?php if (is_404()) : ?>
                
                	<h1 class="main-title"><?php echo $content_title; ?></h1>
        
                    <h4><span class="theme-icon-attention"></span><?php echo $title; ?></h4>
                           
                    <div class="form-404">    
                        <form method="get" class="form-search" action="<?php echo home_url(); ?>/">                             
                            <input class="search-query form-control" name="s" id="s" type="text" placeholder="<?php _e('keyword(s)',IT_TEXTDOMAIN); ?>">                                      
                        </form> 
                    </div>            
                
                <?php elseif($template=='template-authors.php') : ?>
                
                	<h1 class="main-title"><?php echo get_the_title(); ?></h1>
                        
                    <?php echo it_get_content($article_title); ?>
                    
					<?php echo it_get_author_loop(); #get authors and display loop ?>                    
                    
                <?php elseif (have_posts()) : ?>
            
					<?php while (have_posts()) : the_post(); ?>
                    
                        <div id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemscope itemtype="<?php echo $item_type; ?>">
                        
                        	<?php do_action('it_before_single_main', it_get_setting('ad_main_before'), 'before-main'); ?>
                            
                            <?php if($sharing_position=='above_image') echo '<div class="share-wrapper non-control-bar">' . it_get_sharing($sharingargs) . '</div>'; ?>
                        
                            <div class="image-container">
                                
                                <button type="button" class="video_trailer_btn btn btn-dark">Trailer</button>
                                <button type="button" class="video_featured_btn btn btn-dark">Featured Video</button>
                                
                                <?php 
                                $current_post_id = get_the_ID();
                               // echo $current_post_id;
                                $tv_featured_video_custom = get_post_meta( $current_post_id, '_featured_video_custom', true);
                                $tv_trailer_video = get_post_meta( $current_post_id, '_tv_trailer', true);
                                //echo $tv_featured_video_custom;
                                //print_r($tv_featured_video);
                                //print_r($tv_trailer_video);
                                ?>
                                
                                <div class="video_trailer" style="display: block;">
                                	<?php if($tv_trailer_video){ ?>
                                	  <video width="100%" controls>
									  <source src="<?php echo $tv_trailer_video; ?>" type="video/mp4">
									  Your browser does not support HTML5 video.
									</video>
									<?php }else{ echo "No video found"; } ?>
                                </div>
                            	
                                <div class="video_featured" style="display: none;">
                                	<?php if($tv_featured_video_custom){ ?>
                                	 <video width="100%" controls>
									  <source src="<?php echo $tv_featured_video_custom; ?>" type="video/mp4">
									  Your browser does not support HTML5 video.
									</video>
									 <?php } else{ echo "No video found"; } ?>
                                </div>
                               

                                <script type="text/javascript">
                                	jQuery(".video_trailer_btn").click(function(){
									  jQuery(".video_trailer").show();
									  jQuery(".video_featured").hide();
									});
									jQuery(".video_featured_btn").click(function(){
									  jQuery(".video_featured").show();
									  jQuery(".video_trailer").hide();
									});
                                </script>

                                <?php #featured video
                                if(!$disable_video) {                                    
                                    do_action('it_before_featured_video', it_get_setting('ad_video_before'), 'before-video');									
                                    if(!empty($video)) echo it_video($videoargs); 									
                                    do_action('it_after_featured_video', it_get_setting('ad_video_after'), 'after-video');                                    
                                }                                
                                #featured image
                                if($thumbnail!='none' && has_post_thumbnail()) {                                 
                                    do_action('it_before_featured_image', it_get_setting('ad_image_before'), 'before-image');                                        
                                    echo it_featured_image($imageargs);                               
                                    do_action('it_after_featured_image', it_get_setting('ad_image_after'), 'after-image');                                    
                                } ?>
                                
                            </div>

                            <?php 

                            	/* grab the url for the full size featured image */
						        $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
						  
						        /* link thumbnail to full size image for use with lightbox*/
						        echo '<img src="'.esc_url($featured_img_url).'" >'; 
						            
					        ?>                            
                            
                            <?php #contents menu
                            if($contents_menu_display) {                                
                                do_action('it_before_contents_menu', it_get_setting('ad_contents_menu_before'), 'before-contents-menu');								
                                echo it_get_contents_menu(get_the_ID(), $disabled_menu_items);								
                                do_action('it_after_contents_menu', it_get_setting('ad_contents_menu_after'), 'after-contents-menu');                                
                            }
							
                            #details
                            if($details_position=='top') {                                
                                do_action('it_before_details', it_get_setting('ad_details_before'), 'before-details');								
                                echo it_get_details(get_the_ID(), $overlay_image[0], $isreview); 								
                                do_action('it_after_details', it_get_setting('ad_details_after'), 'after-details');                                
                            }
                            #rating criteria
                            if($ratings_position=='top') {                                
                                do_action('it_before_criteria', it_get_setting('ad_criteria_before'), 'before-criteria');								
                                echo it_get_criteria(get_the_ID(), $overlay_image[0]);								
                                do_action('it_after_criteria', it_get_setting('ad_criteria_after'), 'after-criteria');                                
                            }
                            #reactions
                            if($reactions_position=='top') {                                
                                do_action('it_before_reactions', it_get_setting('ad_reactions_before'), 'before-reactions');								
                                echo it_get_reactions(get_the_ID());								
                                do_action('it_after_reactions', it_get_setting('ad_reactions_after'), 'after-reactions');                                
                            }
							
							if($sharing_position=='above_content') echo '<div class="share-wrapper non-control-bar">' . it_get_sharing($sharingargs) . '</div>';
							$user_role = get_post_meta(get_the_ID(),'post_type',true);	

							//$user_id = get_current_user_id();
							$current_post_id = get_the_ID();  
							//print_r($current_post_id);

							$tv_cast = get_post_meta( $current_post_id, 'tv_cast',true );
							$tv_image = get_post_meta( $current_post_id, 'tv_cast_image',true );
							$tv_cast_bio = get_post_meta( $current_post_id, 'tv_cast_bio',true );

							//print_r($tv_cast);
							//print_r($tv_image);
							//print_r($tv_cast_bio);

							$tv_image_arr= explode(',',$tv_image);
							$tv_cast_arr= explode(',',$tv_cast);
							$tv_cast_bio_arr= explode(',',$tv_cast_bio);

							//$user_data = array_merge_recursive($tv_cast_arr,$tv_image_arr,$tv_cast_bio_arr);
							echo '<div class="author-info clearfix author_main row">';



							$image_cast_arr = array_combine($tv_cast_arr, $tv_image_arr);
							//print_r($image_cast_arr);

							echo '<div class="image_cast_main col-md-2">';

							$n = 1; 
								foreach($image_cast_arr as $tv_cast=>$tv_image){
									echo '<div onclick="show_bio(this)" current_index="'.$n.'" lass="author-image thumbnail inner'.$n.'" >
										<a class="info" title="" data-original-title="'.$tv_cast.'"><img alt="" src="'.$tv_image.'" srcset="" class="avatar avatar-70 photo avatar-default img-circle" width="100" height="100"></a>
										<div class="cast_designation">'.$tv_cast.'</div>
										</div>';
								$n++;		
									
								}
							echo '</div>';	
									
							$m= 1;
							echo '<div class="cast_bio_main">';
							foreach($tv_cast_bio_arr as $tv_cast_bio){
								echo '<p style="display:none;" class="bio_inner bio'.$m.'">'.$tv_cast_bio.'</p>';
								$m++;
							}
							echo '</div>';
							echo '</div>';	
							?>

								<script type="text/javascript">
									//jQuery( document ).ready(function() {
										console.log("ready..");
										function show_bio(value){
											var clicked_class = jQuery(value).attr('current_index');
											console.log(clicked_class);
											jQuery('.bio'+clicked_class).toggle();
										}
									//});
								</script>

							<?php
							//}
                            #content
                            do_action('it_before_content', it_get_setting('ad_content_before'), 'before-content');
							
							if($affiliate_position=='before-content') echo it_get_affiliate_code(get_the_ID());                                
                           echo it_get_content($article_title);							
                           do_action('it_after_content', it_get_setting('ad_content_after'), 'after-content');
							
							if($affiliate_position=='after-content') echo it_get_affiliate_code(get_the_ID());
                            
                            #details
                            if($details_position=='bottom') {                                
                                do_action('it_before_details', it_get_setting('ad_details_before'), 'before-details');								
                                echo it_get_details(get_the_ID(), $overlay_image[0], $isreview); 								
                                do_action('it_after_details', it_get_setting('ad_details_after'), 'after-details');                                 
                            }
                            #rating criteria  
                            if($ratings_position=='bottom') {                                
                                // do_action('it_before_criteria', it_get_setting('ad_criteria_before'), 'before-criteria');								
                                // echo it_get_criteria(get_the_ID(), $overlay_image[0]);								
                                // do_action('it_after_criteria', it_get_setting('ad_criteria_after'), 'after-criteria');                                
                            }
                            #reactions
                            if($reactions_position=='bottom') {                                
                               // do_action('it_before_reactions', it_get_setting('ad_reactions_before'), 'before-reactions');								
                               // echo it_get_reactions(get_the_ID());								
                                //do_action('it_after_reactions', it_get_setting('ad_reactions_after'), 'after-reactions');                                 
                            }
                            #post info
                            if(!$disable_postinfo) {                                
                                //do_action('it_before_postinfo', it_get_setting('ad_postinfo_before'), 'before-postinfo');
                                //echo it_get_post_info(get_the_ID());
                                //do_action('it_after_postinfo', it_get_setting('ad_postinfo_after'), 'after-postinfo');                                
                            }
							
							if($sharing_position=='below_content') //echo '<div class="share-wrapper non-control-bar">' . it_get_sharing($sharingargs) . '</div>';
							
                            #recommended
                            if(!$disable_recommended){                                
                                do_action('it_before_recommended', it_get_setting('ad_recommended_before'), 'before-recommended');
                                echo it_get_recommended(get_the_ID());
                                do_action('it_after_recommended', it_get_setting('ad_recommended_after'), 'after-recommended');                                
                            }
                            #comments
                            if(!$disable_comments && comments_open()) {                                
                                //do_action('it_before_comments', it_get_setting('ad_comments_before'), 'before-comments');
                                //comments_template();
                                //do_action('it_after_comments', it_get_setting('ad_comments_after'), 'after-comments');                                
                            } ?> 
                            
                            <?php //do_action('it_after_single_main', it_get_setting('ad_main_after'), 'after-main'); ?>                             
                            <?php $credits_show = get_post_meta( get_the_ID(), '_tv_credits', true); ?>
                            <blockquote class="quote-card ">
				              <p>
				                <?php echo $credits_show; ?> 
				              </p>
				            </blockquote>                            
                            
                            <!-- Jssor Slider Starts-->
                            <script type="text/javascript">
						        window.jssor_1_slider_init = function() {

						            var jssor_1_options = {
						              $AutoPlay: 1,
						              $AutoPlaySteps: 1,
						              $SlideDuration: 160,
						              $SlideWidth: 150,
						              $SlideSpacing: 3,
						              $ArrowNavigatorOptions: {
						                $Class: $JssorArrowNavigator$,
						                $Steps: 4
						              },
						              $BulletNavigatorOptions: {
						                $Class: $JssorBulletNavigator$
						              }
						            };

						            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

						            /*#region responsive code begin*/

						            var MAX_WIDTH = 750;

						            function ScaleSlider() {
						                var containerElement = jssor_1_slider.$Elmt.parentNode;
						                var containerWidth = containerElement.clientWidth;

						                if (containerWidth) {

						                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

						                    jssor_1_slider.$ScaleWidth(expectedWidth);
						                }
						                else {
						                    window.setTimeout(ScaleSlider, 30);
						                }
						            }

						            ScaleSlider();

						            $Jssor$.$AddEvent(window, "load", ScaleSlider);
						            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
						            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
						            /*#endregion responsive code end*/
						        };
						    </script>
						    <style>
						        /*jssor slider loading skin spin css*/
						        .jssorl-009-spin img {
						            animation-name: jssorl-009-spin;
						            animation-duration: 1.6s;
						            animation-iteration-count: infinite;
						            animation-timing-function: linear;
						        }

						        @keyframes jssorl-009-spin {
						            from { transform: rotate(0deg); }
						            to { transform: rotate(360deg); }
						        }

						        /*jssor slider bullet skin 057 css*/
						        .jssorb057 .i {position:absolute;cursor:pointer;}
						        .jssorb057 .i .b {fill:none;stroke:#fff;stroke-width:2000;stroke-miterlimit:10;stroke-opacity:0.4;}
						        .jssorb057 .i:hover .b {stroke-opacity:.7;}
						        .jssorb057 .iav .b {stroke-opacity: 1;}
						        .jssorb057 .i.idn {opacity:.3;}

						        /*jssor slider arrow skin 073 css*/
						        .jssora073 {display:block;position:absolute;cursor:pointer;}
						        .jssora073 .a {fill:#ddd;fill-opacity:.7;stroke:#000;stroke-width:160;stroke-miterlimit:10;stroke-opacity:.7;}
						        .jssora073:hover {opacity:.8;}
						        .jssora073.jssora073dn {opacity:.4;}
						        .jssora073.jssora073ds {opacity:.3;pointer-events:none;}
						        
						    </style>
						    <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:750px;height:180px;overflow:hidden;visibility:hidden;">
						        <!-- Loading Screen -->
						        <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
						            <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="img/spin.svg" />
						        </div>
						        <div class="c_slides_main" data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:750px;height:180px;overflow:hidden;">
						            
						        	<?php
						        	$current_post_id = get_the_ID(); 
						        	$carousel_img_ids = get_post_meta( $current_post_id, '_new_photo_carousel',true);					   
						        	$carousel_img_ids_exp = explode(",",$carousel_img_ids);
						        	
						        	
						        	foreach ($carousel_img_ids_exp as $att_id) {
									    
									    $attachment_url = wp_get_attachment_url( $att_id );
									    echo '<div class="car_img_inner"><img data-u="image" src="'.$attachment_url.'" /></div>';  
									}

						        	?>
						        </div>
						        <!-- Bullet Navigator -->
						        <div data-u="navigator" class="jssorb057" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
						            <div data-u="prototype" class="i" style="width:16px;height:16px;">
						                <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
						                    <circle class="b" cx="8000" cy="8000" r="5000"></circle>
						                </svg>
						            </div>
						        </div>
						        <!-- Arrow Navigator -->
						        <div data-u="arrowleft" class="jssora073" style="width:50px;height:50px;top:0px;left:30px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
						            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
						                <path class="a" d="M4037.7,8357.3l5891.8,5891.8c100.6,100.6,219.7,150.9,357.3,150.9s256.7-50.3,357.3-150.9 l1318.1-1318.1c100.6-100.6,150.9-219.7,150.9-357.3c0-137.6-50.3-256.7-150.9-357.3L7745.9,8000l4216.4-4216.4 c100.6-100.6,150.9-219.7,150.9-357.3c0-137.6-50.3-256.7-150.9-357.3l-1318.1-1318.1c-100.6-100.6-219.7-150.9-357.3-150.9 s-256.7,50.3-357.3,150.9L4037.7,7642.7c-100.6,100.6-150.9,219.7-150.9,357.3C3886.8,8137.6,3937.1,8256.7,4037.7,8357.3 L4037.7,8357.3z"></path>
						            </svg>
						        </div>
						        <div data-u="arrowright" class="jssora073" style="width:50px;height:50px;top:0px;right:30px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
						            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
						                <path class="a" d="M11962.3,8357.3l-5891.8,5891.8c-100.6,100.6-219.7,150.9-357.3,150.9s-256.7-50.3-357.3-150.9 L4037.7,12931c-100.6-100.6-150.9-219.7-150.9-357.3c0-137.6,50.3-256.7,150.9-357.3L8254.1,8000L4037.7,3783.6 c-100.6-100.6-150.9-219.7-150.9-357.3c0-137.6,50.3-256.7,150.9-357.3l1318.1-1318.1c100.6-100.6,219.7-150.9,357.3-150.9 s256.7,50.3,357.3,150.9l5891.8,5891.8c100.6,100.6,150.9,219.7,150.9,357.3C12113.2,8137.6,12062.9,8256.7,11962.3,8357.3 L11962.3,8357.3z"></path>
						            </svg>
						        </div>
						    </div>
						    <script type="text/javascript">jssor_1_slider_init();
						    </script>
						    <!-- #endregion Jssor Slider End -->
                            <!-- Jssor Slider Ends-->

                        </div>
                    
                    <?php endwhile; ?> 
                
                <?php endif; ?> 
                
                <?php wp_reset_query(); ?>
                    
            </div>  
            
           
        
        
        	    <?php if($sidebar_position=='sidebar-right') { ?>    
			    	<div class="<?php echo $csscol3; ?>">
			       		<?php echo it_widget_panel($sidebar, $sidebar_position); ?>		            
			        </div>		                    
		    	<?php } ?>
		</div>    	
    </div>

</div>

<?php if($layout=='billboard') { ?>

	</div>
    
</div>

<?php } ?>

<?php do_action('it_after_content_page'); ?>

<?php wp_reset_query(); ?>