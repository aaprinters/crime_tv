<?php 
/************
 * Template Name: Awards
 ***********/

get_header(); // show header ?>


<?php
     $feat_image3 = '';
                if (has_post_thumbnail( $post->ID ) ){ 
                $feat_image3 = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
    }
    ?>



<style type="text/css">
	.it-ad{
		display:none !important;
	}
	#it-ad-footer {
        display: none;
    }
    #awards{
    	width:100%;
    	float:left;
    	height:auto;
    	/*background: url(<?php echo $feat_image3; ?>) no-repeat;
	   background-size:cover;
	   background-position: center 20%;*/
       background:#000;
    }
    h1.award-title{
    color: #FFF;
    text-shadow: 1px 2px 2px rgba(0,0,0,1);
    font-size: 70px;
    line-height: 70px;
    font-family: 'Roboto Condensed', sans-serif!important;
    font-weight: 700;
    text-align: center;
    text-transform: uppercase;
    margin: 70px 50px 80px 70px;
    }
    .list3{
    	padding-left:0px;
    }
    .list3 li{
    	list-style: none;
        display: inline-block;
        margin-right: 5px;
        color:#fff;
    }
    a{
    	color:#fff !important;
    }
    h3{
    	    text-align: center;
    }
    img{
    	    border-radius: 50%;
    }
    a.layer-link2 {
   }
   .p-title{
   	width:100%;
   	float:let;
   	height:auto;
   }
   .article-image-wrapper{
   	height: 255px;
    width: 255px;
    overflow: hidden;
    position: relative;
   }
   .layer-link2{
    width: 100%;
    height: 100%;
    position: absolute;
    top:0px;
    left:0px;
    opacity: 0;
    background-color: rgba(0,0,0,0.75);
    -webkit-transition: all 0.7s ease;
    transition: all 0.7s ease;
    border-radius:50%;
}
.layer-link2:hover{
	opacity:1;

}
.top-image{
    width:100%;
    float:left;
    height: auto;
}
.top-image img{
    width: 100%;
    border-radius: 0%;
}
.award-submit{
    background: #000;
    width: 100%;
    float: left;
    text-align: center;
}
.award-submit a{
    width: 45%;
    background: #9d7413;
    float: left;
    margin: 20px 27%;
    height: auto;
    font-size: 28px;
    padding: 25px 0px;
}

@media only screen and (max-width:700px) {

h1.award-title {
    font-size: 40px;
    line-height: 54px;
    font-weight: 700;
    margin: 15px 5px 15px 5px;
}
.article-image-wrapper {
    height: auto;
    width: 100%;
}
.award-submit{
    text-align: center;
}
.award-submit a {
    width: 40%;
    background: #9d7413;
    float: left;
    margin: 10px 30%;
    height: auto;
    font-size: 20px;
    padding: 10px 10px;
}

}
</style>

<section class="top-image">
    <img src="<?php echo $feat_image3; ?>">
</section>


<section id="awards">
	<div class="container">

		<h1 class="award-title"><?php the_title();?></h1>

<?php

					$args = array(

					'post_type' => 'award',

					  'orderby' => 'name',

					  'order' => 'ASC',	

					  );					  

					  $query = new WP_Query($args);

						if($query->have_posts()) : while ($query->have_posts()) : $query->the_post();



			  ?>
              <?php
     $feat_image3 = '';
                if (has_post_thumbnail( $post->ID ) ){ 
                $feat_image3 = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
    }
    ?>





<div class="article-panel add-active clearfix category-2 col-md-3 grid-3" style="height: 392px;">


	    <div class="article-image-wrapper">
	    	<div class="article-image">
	    		
	    		   <a href="<?php the_permalink(); ?>">
	    		   	    <div class="layer-link2">&nbsp;</div>
	    		   		<img width="360" height="240" src="<?php echo $feat_image3; ?>" class="attachment-grid-3 size-grid-3 wp-post-image" alt="" title="<?php echo $feat_image3; ?>">
	    		   	</a>

               <!-- <ul class="list3">
	    	   <?php 
               $category = get_the_category( $post->ID );
	               for ($i = 0; $i <= $post; $i++) {
	               	   echo "<li>";
	                   echo $category[ $i]->cat_name;
	                   echo "</li>";
	                }

               ?>
               </ul> -->

               



	    		 </div>
	    	</div>
             
            <div class="p-title">
	    	<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
	        </div>

	    		   				

	    	<div class="article-info">
	    		<div class="excerpt">
	    			<div class="excerpt-text"><?php echo substr(strip_tags($post->post_content), 0,400); ?></div>
	    		</div>
	    	</div>


	<!-- infinite grid starts here.................. -->

	 <div id="infinite-grid" class="articles gradient post-grid">
    
            <div id="infinite-grid-container" class="<?php echo $csscol2; ?>" data-currentquery='<?php echo $current_query_encoded; ?>'>
            
            	<?php echo it_archive_title(); ?>
            
            	<!-- <?php echo it_get_sortbar($sortbarargs); ?> -->
                
                <div class="content-inner">
                
                	<div class="loading load-sort"><span class="theme-icon-spin2"></span></div>
                
                    <div class="loop grid row">
                    
                        <?php $loop = it_loop($args, $format); echo $loop['content']; ?>
                        
                    </div>
                    
                    <div class="loading load-infinite"><span class="theme-icon-spin2"></span></div>
                    
                </div>
                
                <?php if(!$disable_loadmore) echo it_get_loadmore($loadmoreargs); ?>
                
                <div class="last-page"><?php _e('End of the line!',IT_TEXTDOMAIN); ?></div>
            
            </div>
            
        </div>

        <!-- infinite grid ends here.................. -->





	    		   			</div>










     







                      <?php endwhile; 

						 wp_reset_postdata();?>	

					    <?php endif; ?> 




</div>
<!-- container ends..... -->
</section> 
<!-- section ends.....  -->


<section class="award-submit">
    <?php the_content(); ?>
</section>






<?php get_footer(); // show footer ?>